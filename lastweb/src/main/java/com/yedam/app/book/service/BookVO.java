package com.yedam.app.book.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class BookVO {
	String bookNum;
	String bcaNum;
	String scaNum;
	String bookTitle;
	String bookIntro;
	String bookWriter;
	int bookRating;
	int bookPrice;
	String attachNum;
	int bookStock;
	
	String clNum;
	String clThumbnail;
	String clName;
	int clGrade;
	int clAmount;
	String clIntro;
	@DateTimeFormat(pattern="yyyy-MM-dd") // yyyy/MM/dd
	Date clStart;
	String clReport;
	String mbId;
	String mbNick;

}
