package com.yedam.app.book.mapper;

import java.util.List;

import com.yedam.app.book.service.BookReviewVO;
import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.payment.service.StockVO;
import com.yedam.app.study.service.BigCateVO;
import com.yedam.app.study.service.CateVO;

public interface BookMapper {
	
	// 도서 목록
	public List<BookVO> allBook();
	// 도서 목록 (페이징)
	public List<BookVO> getAllList(Criteria criteria);
	public int getBookListTotal();
	
	// 도서 목록 검색
	public List<BookVO> searchBook(String keyword, Criteria criteria);
	public int getSeachTotal(String keyword);
	
	// 도서 목록 카테고리 검색
	public List<BookVO> searchCate(String bigkeyword, String keyword, Criteria criteria);
	public int getCateTotal(String keyword, String bigkeyword);
	
	// 도서 상세
	public BookVO getBook(String bookNum);
	
	// 도서 후기
	public List<BookVO> getReview(String bookNum, Criteria criteria);
	public int getBookRevTotal(String bookNum);
	
	//도서 후기 등록
	public int addRev(BookReviewVO bookRev);
	
	// 도서 후기 수정
	public int bookRevModify(BookReviewVO bookRevVO);
	
	// 도서 후기 삭제
	public int bookRevDel(String reviewNum);
	
	// 관리자 도서 목록
	public List<BookVO> adminAllBookList(Criteria criteria);
	
	// 관리자 도서 등록
	public int insertBook(BookVO vo);
	
	// 관리자 도서 수정
	public int modifyBook(BookVO bookVO);

	// 관리자 도서 삭제
	public int remove(String bookNum);
	public int deleteBook(String bookNum);

	// 관리자 도서 입출고 - 도서 상세
	public List<StockVO> getStock(String bookNum);
	
	// 관리자 도서 입출고 - 도서 입고량 추가
	public int addStock(StockVO stock);
	
	// 관리자 도서 입출고 - 도서 재고 업데이트
	public int updateStock(BookVO book);
	
	// 관리자 도서 입출고 - 카테고리 검색
	public List<BookVO> bookInOutCate(String keyword, String bigkeyword);

	//카테고리 대분류
	public List<BigCateVO> bigCateList();

	//카테고리 소분류
	public List<CateVO> cateList();


}
