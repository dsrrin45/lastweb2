package com.yedam.app.book.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class BookReviewVO {

	String goodsNum;
	String bookNum;
	int reviewGrade;
	String reviewContent;
	String reviewNum;
	String mbId;
	String reportNum;
	String def;
	@DateTimeFormat(pattern="yyyy-MM-dd") // yyyy/MM/dd
	Date reviewTime;
	String mbProfile;
}
