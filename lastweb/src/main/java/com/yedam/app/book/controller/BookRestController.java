package com.yedam.app.book.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.board.service.BoardService;
import com.yedam.app.book.service.BookReviewVO;
import com.yedam.app.book.service.BookService;
import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.member.service.MbVO;
import com.yedam.app.payment.service.CartService;
import com.yedam.app.payment.service.StockVO;
import com.yedam.app.payment.service.WishVO;

@Controller
public class BookRestController {
	@Autowired
	BookService bookService;
	
	@Autowired
	CartService cartService;
	
	@Autowired
	BoardService boardService;
	
	// 도서 목록
	@GetMapping("/bookList")
	public String AllBookList(Criteria criteria, Model model, WishVO wish) {
		model.addAttribute("bookList", bookService.getAllList(criteria));
		model.addAttribute("pageMaker", new PageDTO(bookService.getBookListTotal(), 10, criteria));
		// model.addAttribute("bookWish", cartService.WishCheck(wish));
		
		return "/book/bookList";
	}
		
	// 도서 목록 검색
	@GetMapping("/searchBook")
	 public String search(@RequestParam("keyword") String keyword, Criteria criteria, Model model) {
	    model.addAttribute("bookList", bookService.searchBook(keyword, criteria));
	    model.addAttribute("pageMaker", new PageDTO(bookService.getSeachTotal(keyword), 10, criteria));

	    return "/book/bookList";
	 }
	
	// 도서 목록 카테고리 검색
	@GetMapping("/bookCate")
	public String catesearch(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Criteria criteria, Model model) {
	    model.addAttribute("bookList", bookService.searchCate(keyword, bigkeyword, criteria));
	    model.addAttribute("pageMaker", new PageDTO(bookService.getCateTotal(keyword, bigkeyword), 10, criteria)); 
	    return "/book/bookList";
	} 
	
	// 도서 상세
	@GetMapping("/bookInfo/{bookNum}")
	public String bookInfoList(@PathVariable("bookNum") String bookNum, Model model, Criteria criteria) {
		model.addAttribute("book", bookService.getBook(bookNum));
		model.addAttribute("bookReviw", bookService.getReview(bookNum, criteria));
	    model.addAttribute("pageMaker", new PageDTO(bookService.getBookRevTotal(bookNum), 10, criteria));

		//model.addAttribute("wishState", cartService.getWishSt(bookNum));
		return "/book/bookInfoList";
	}
	
	// 도서 후기등록
	@PostMapping("/bookRevAdd")
	public String addBookRev(BookReviewVO bookRevVO, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			bookService.addBookRev(bookRevVO);
			return "redirect:bookInfo/" + bookRevVO.getBookNum();
		} else {
			return "redirect:/login";
		}
	}
	
	// 도서 후기수정
	@ResponseBody
	@PostMapping("/bookRevModify")
	public String bookRevModify(BookReviewVO bookRevVO) {
		bookService.bookRevModify(bookRevVO);
		return "/book/bookInfoList/";
	}
	
	// 도서 후기삭제
	@ResponseBody
	@GetMapping("/bookRevDel/{reviewNum}")
	public void bookRevDel(@PathVariable("reviewNum") String reviewNum) {
		bookService.bookRevDel(reviewNum);
	}
	

	// 관리자 도서 목록
	@GetMapping("/admin/adminBook")
	public String adminBookList(Criteria criteria, Model model) {
		model.addAttribute("bookList", bookService.adminGetAllList(criteria));
		model.addAttribute("pageMaker", new PageDTO(bookService.getBookListTotal(), 10, criteria));
	    model.addAttribute("cateList", bookService.cateList());
		model.addAttribute("bigCateList", bookService.bigCateList());
		return "admin/adminBookList";
	}
	
	// 관리자 도서 목록 - 카테고리 검색
	@GetMapping("/admin/bookListCate")
	public String adminBookListCate(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Model model, Criteria criteria) {
		model.addAttribute("bookList", bookService.searchCate(keyword, bigkeyword, criteria));
	    model.addAttribute("pageMaker", new PageDTO(bookService.getCateTotal(keyword, bigkeyword), 10, criteria)); 
	    model.addAttribute("cateList", bookService.cateList());
		model.addAttribute("bigCateList", bookService.bigCateList());
	    return "admin/adminBookList";
	} 
	
	
	// 관리자 도서 상세
	@GetMapping("/admin/adminBookView")
	public String adminBookView(@RequestParam String bookNum, Model model, Criteria criteria) {
		model.addAttribute("book", bookService.getBook(bookNum));
		model.addAttribute("pageMaker", new PageDTO(bookService.getBookListTotal(), 10, criteria));
		return "admin/adminBookView";
	}
	
	
    // 관리자 도서 등록페이지
    @GetMapping("/admin/addBook")
    public String addBook(Model model) {
		model.addAttribute("cateList", bookService.cateList());
		model.addAttribute("bigCateList", bookService.bigCateList());
        return "admin/adminBookForm";
    }
    
    // 관리자 도서 등록
	@PostMapping("/admin/addBook")
	public String addBook(BookVO bookVO) {
		bookService.addBook(bookVO);
		return "redirect:/admin/adminBook";
	}
	
	// 관리자 도서 수정 페이지
	@GetMapping("/admin/adminModifyBook")
	public String adminModifyBook(@RequestParam String bookNum, Model model, Criteria criteria) {
		model.addAttribute("bookView", bookService.getBook(bookNum));
		model.addAttribute("pageMaker", new PageDTO(boardService.getNoticeListTotal(), 10, criteria));
		model.addAttribute("cateList", bookService.cateList());
		model.addAttribute("bigCateList", bookService.bigCateList());
		return "admin/adminBookModify";
	}
	
	// 관리자 도서 수정
	@PostMapping("/admin/adminModifyBook")
	public String adminModifyBook(BookVO bookVO) {
		bookService.modifyBook(bookVO);
		return "redirect:adminBookView?bookNum=" + bookVO.getBookNum();
	}
	
	
	// 관리자 도서 삭제
	@GetMapping("/admin/adminDeleteBook/{bookNum}") 
	public String adminDeleteBook(@PathVariable("bookNum") String bookNum) {
		bookService.deleteBook(bookNum);
		return "redirect:/admin/adminBook";
	}
	
	
	// 관리자 도서 입출고 - 도서 목록
	@GetMapping("/admin/bookInOut")
	public String adminBookInOut(Model model) {
		model.addAttribute("bookList", bookService.allBook());
		model.addAttribute("cateList", bookService.cateList());
		model.addAttribute("bigCateList", bookService.bigCateList());
		return "admin/adminBookInOut";
	}
	
	// 관리자 도서 입출고 - 카테고리 검색
		@GetMapping("/admin/bookInOutCate")
		public String bookInOutCate(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Model model) {
		    model.addAttribute("bookList", bookService.bookInOutCate(keyword, bigkeyword));
		    model.addAttribute("cateList", bookService.cateList());
			model.addAttribute("bigCateList", bookService.bigCateList());
		    return "admin/adminBookInOut";
		} 
	
	// 관리자 도서 입출고 - 도서 상세
	@RequestMapping(value = "/admin/getStock/{bookNum}")
	@ResponseBody
	public List<StockVO> getStock(@PathVariable("bookNum") String bookNum) {
		System.out.println(bookNum);
		List<StockVO> stock;
		stock = bookService.getStock(bookNum);
		return stock;
	}

	// 관리자 도서 입고량 추가
	@ResponseBody
	@PostMapping("/admin/insertStock")
	public StockVO insertStock(StockVO stock, BookVO book, Model model) {
		bookService.addStock(stock);
		bookService.updateStock(book);
		return stock;
	}
}

   