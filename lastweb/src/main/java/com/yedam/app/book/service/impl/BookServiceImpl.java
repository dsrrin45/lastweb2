package com.yedam.app.book.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yedam.app.book.mapper.BookMapper;
import com.yedam.app.book.service.BookReviewVO;
import com.yedam.app.book.service.BookService;
import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.payment.service.StockVO;
import com.yedam.app.study.mapper.StudyMapper;
import com.yedam.app.study.service.BigCateVO;
import com.yedam.app.study.service.CateVO;

@Service
public class BookServiceImpl implements BookService {
	
	@Autowired
	BookMapper bookMapper;
	
	@Autowired
	StudyMapper studyMapper;
	
	
	// 도서 목록
	@Override
	public List<BookVO> allBook() {
		return bookMapper.allBook();
	}
	
	// 도서 목록 (페이징)
	@Override
	public List<BookVO> getAllList(Criteria criteria) {
		return bookMapper.getAllList(criteria);
	}
	@Override
	public int getBookListTotal() {
		int getBookListTotal = bookMapper.getBookListTotal();
		return getBookListTotal;
	}
	
	// 도서 목록 검색
	@Override
	public List<BookVO> searchBook(String keyword, Criteria criteria) {
		return bookMapper.searchBook(keyword, criteria);
	}
	@Override
	public int getSeachTotal(String keyword) {
		return bookMapper.getSeachTotal(keyword);
	}
	
	// 도서 목록 카테고리 검색
	@Override
	public List<BookVO> searchCate(String keyword, String bigkeyword, Criteria criteria) {
		return bookMapper.searchCate(bigkeyword, keyword, criteria);
	}
	@Override
	public int getCateTotal(String keyword, String bigkeyword) {
		return bookMapper.getCateTotal(keyword, bigkeyword);
	}
	
	
	// 도서 상세
	@Override
	public BookVO getBook(String bookNum) {
		return bookMapper.getBook(bookNum);
	}
	
	// 도서 후기
	@Override
	public List<BookVO> getReview(String goodsNum, Criteria criteria) {
		return bookMapper.getReview(goodsNum, criteria);
	}
	@Override
	public int getBookRevTotal(String bookNum) {
		return bookMapper.getBookRevTotal(bookNum);
	}
	
	// 도서 후기 등록
	@Override
	public int addBookRev(BookReviewVO bookRev) {
		return bookMapper.addRev(bookRev);
	}
	
	// 도서 후기 수정
	@Override
	public int bookRevModify(BookReviewVO bookRevVO) {
		return bookMapper.bookRevModify(bookRevVO);
	}
	
	// 도서 후기 삭제
	@Override
	public int bookRevDel(String reviewNum) {
		return bookMapper.bookRevDel(reviewNum);
	}

	// 관리자 도서 목록
	@Override
	public List<BookVO> adminGetAllList(Criteria criteria) {
		return bookMapper.adminAllBookList(criteria);
	}
	
	// 관리자 도서 등록
	@Override
	public int addBook(BookVO bookVO) {
		return bookMapper.insertBook(bookVO);
	}

	// 관리자 도서 수정
	@Override
	public int modifyBook(BookVO bookVO) {
		int result = bookMapper.modifyBook(bookVO);
		return result;		
	}

	// 관리자 도서 삭제
	@Override
	public int deleteBook(String bookNum) {
		int result = bookMapper.deleteBook(bookNum);
		return result;		
	}
	
	// 관리자 도서 입출고 - 도서 상세 
	@Override
	public List<StockVO> getStock(String bookNum) {
		return bookMapper.getStock(bookNum);
	}
	
	// 관리자 도서 입출고 - 도서 입고량 추가
	@Override
	public int addStock(StockVO stock) {
		return bookMapper.addStock(stock);
	}
	
	// 관리자 도서 입출고 - 도서 재고 업데이트
	@Override
	public int updateStock(BookVO book) {
		return bookMapper.updateStock(book);
	}
	
	// 관리자 도서 입출고 - 카테고리 검색
	@Override
	public List<BookVO> bookInOutCate(String keyword, String bigkeyword) {
		return bookMapper.bookInOutCate(keyword, bigkeyword);
	}

	//카테고리 대분류
	@Override
	public List<BigCateVO> bigCateList() {
		return studyMapper.bigCateList();
	}

	//카테고리 소분류
	@Override
	public List<CateVO> cateList() {
		return studyMapper.cateList();
	}



	
}
