package com.yedam.app.board.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yedam.app.board.mapper.BoardMapper;
import com.yedam.app.config.Criteria;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class BoardDAO {
	private final BoardMapper boardMapper;
	
	public List<BoardVO> getNoticeList(Criteria cri) {
		return boardMapper.getNoticeList(cri);
	}
	
	public List<BoardVO> getMyQnaList(String mbId, int pageNum, int amount) {
		return boardMapper.getMyQnaList(mbId, pageNum, amount);
	}
	
	public List<BoardVO> getQnaList(Criteria cri) {
		return boardMapper.getQnaList(cri);
	}
}
