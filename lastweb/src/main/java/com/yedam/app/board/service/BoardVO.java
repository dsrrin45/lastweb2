package com.yedam.app.board.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class BoardVO {
	String boNum;
	String boTitle;
	String boContent;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date writeDate;
	String boState;
	String orgNum;
	String boCategory;
	String mbId;
	
	String mbNick;
	String mbProfile;
}