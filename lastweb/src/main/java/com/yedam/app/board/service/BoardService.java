package com.yedam.app.board.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.yedam.app.config.Criteria;

@Service
public interface BoardService {
	// 공지사항 목록
	public List<BoardVO> getNoticeList(Criteria criteria);

	public int getNoticeListTotal();

	// 공지사항 상세
	public BoardVO getNoticeView(String boNum);

	// 공지사항 등록
	public int insertNotice(BoardVO boardVO);

	// 공지사항 수정
	public int modifyNotice(BoardVO boardVO);

	// 공지사항 삭제
	public int deleteNotice(String boNum);

	// 공지사항 목록
	public List<BoardVO> getMyQnaList(String mbId, int pageNum, int amount);

	public List<BoardVO> getQnaList(Criteria criteria);

	public int getMyQnaListTotal(String mbId);

	public int getQnaListTotal();

	// 1:1 문의 상세
	public BoardVO getQnaView(String boNum);

	public List<BoardVO> getQnaCommentList(String boNum);

	// 1:1 문의 등록
	public int insertQna(BoardVO boardVO);

	// 1:1 문의 답변 등록
	public int modifyQnaState(BoardVO boardVO); // 원글 수정

	public int insertQnaAnswer(BoardVO boardVO);

	// 1:1 문의 답변 수정
	public int modifyQnaAnswer(BoardVO boardVO);

	// 자주 묻는 질문 목록
	public List<BoardVO> getFaqList();

	// 공지사항 등록
	public int insertFaq(BoardVO boardVO);
}