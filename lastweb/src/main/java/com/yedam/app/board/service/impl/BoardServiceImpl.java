package com.yedam.app.board.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.board.mapper.BoardMapper;
import com.yedam.app.board.service.BoardService;
import com.yedam.app.board.service.BoardVO;
import com.yedam.app.config.Criteria;

@Service
public class BoardServiceImpl implements BoardService {
	@Autowired
	BoardMapper boardMapper;

	@Override
	public List<BoardVO> getNoticeList(Criteria cri) {
		List<BoardVO> boardList = boardMapper.getNoticeList(cri);
		return boardList;
	}

	@Override
	public int getNoticeListTotal() {
		int getNoticeListTotal = boardMapper.getNoticeListTotal();
		return getNoticeListTotal;
	}

	@Override
	public BoardVO getNoticeView(String boNum) {
		BoardVO boardVO = boardMapper.getNoticeView(boNum);
		return boardVO;
	}

	@Override
	public int insertNotice(BoardVO boardVO) {
		int result = boardMapper.insertNotice(boardVO);
		return result;
	}

	@Override
	public int modifyNotice(BoardVO boardVO) {
		int result = boardMapper.modifyNotice(boardVO);
		return result;
	}

	@Override
	public int deleteNotice(String boNum) {
		int result = boardMapper.deleteNotice(boNum);
		return result;
	}

	@Override
	public List<BoardVO> getMyQnaList(String mbId, int pageNum, int amount) {
		List<BoardVO> qnaList = boardMapper.getMyQnaList(mbId, pageNum, amount);
		return qnaList;
	}
	
	@Override
	public List<BoardVO> getQnaList(Criteria cri) {
		List<BoardVO> qnaList = boardMapper.getQnaList(cri);
		return qnaList;
	}

	@Override
	public int getMyQnaListTotal(String mbId) {
		int getQnaListTotal = boardMapper.getMyQnaListTotal(mbId);
		return getQnaListTotal;
	}
	
	@Override
	public int getQnaListTotal() {
		int getQnaListTotal = boardMapper.getQnaListTotal();
		return getQnaListTotal;
	}

	@Override
	public BoardVO getQnaView(String boNum) {
		BoardVO boardVO = boardMapper.getNoticeView(boNum);
		return boardVO;
	}

	@Override
	public List<BoardVO> getQnaCommentList(String boNum) {
		List<BoardVO> boardVO = boardMapper.getQnaCommentList(boNum);
		return boardVO;
	}

	@Override
	public int insertQna(BoardVO boardVO) {
		int result = boardMapper.insertQna(boardVO);
		return result;
	}

	@Override
	public int modifyQnaState(BoardVO boardVO) {
		int result = boardMapper.modifyQnaState(boardVO);
		return result;
	}

	@Override
	public int insertQnaAnswer(BoardVO boardVO) {
		int result = boardMapper.insertQnaAnswer(boardVO);
		return result;
	}

	@Override
	public int modifyQnaAnswer(BoardVO boardVO) {
		int result = boardMapper.modifyQnaAnswer(boardVO);
		return result;
	}

	@Override
	public List<BoardVO> getFaqList() {
		List<BoardVO> boardVO = boardMapper.getFaqList();
		return boardVO;
	}

	@Override
	public int insertFaq(BoardVO boardVO) {
		int result = boardMapper.insertFaq(boardVO);
		return result;
	}
}