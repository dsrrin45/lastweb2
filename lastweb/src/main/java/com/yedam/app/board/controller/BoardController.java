package com.yedam.app.board.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.board.service.BoardService;
import com.yedam.app.board.service.BoardVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.member.service.MbVO;

@Controller
public class BoardController {
	@Autowired
	BoardService boardService;

	// 공지사항 목록
	@GetMapping("/noticeList")
	public String noticeList(Criteria criteria, Model model) {
		model.addAttribute("noticeList", boardService.getNoticeList(criteria));
		model.addAttribute("pageMaker", new PageDTO(boardService.getNoticeListTotal(), 10, criteria));
		return "customer/noticeList";
	}

	@GetMapping("/admin/adminNoticeList")
	public String adminNoticeList(Criteria criteria, Model model) {
		model.addAttribute("noticeList", boardService.getNoticeList(criteria));
		model.addAttribute("pageMaker", new PageDTO(boardService.getNoticeListTotal(), 10, criteria));
		return "admin/adminNoticeList";
	}

	// 공지사항 상세
	@GetMapping("/noticeView")
	public String noticeView(@RequestParam String boNum, Model model, Criteria criteria) {
		model.addAttribute("noticeView", boardService.getNoticeView(boNum));
		model.addAttribute("pageMaker", new PageDTO(boardService.getNoticeListTotal(), 10, criteria));
		return "customer/noticeView";
	}

	@GetMapping("/admin/adminNoticeView")
	public String adminNoticeView(@RequestParam String boNum, Model model, Criteria criteria) {
		model.addAttribute("noticeView", boardService.getNoticeView(boNum));
		model.addAttribute("pageMaker", new PageDTO(boardService.getNoticeListTotal(), 10, criteria));
		return "admin/adminNoticeView";
	}

	// 공지사항 등록
	@GetMapping("/admin/adminNoticeWrite")
	public String adminNoticeWrite() {
		return "admin/adminNoticeWrite";
	}

	@PostMapping("/admin/adminNoticeWrite")
	public String adminNoticeWrite(BoardVO boardVO) {
		boardService.insertNotice(boardVO);
		return "redirect:adminNoticeList";
	}

	// 공지사항 수정
	@GetMapping("/admin/adminNoticeModify")
	public String adminNoticeModify(@RequestParam String boNum, Model model, Criteria criteria) {
		model.addAttribute("noticeView", boardService.getNoticeView(boNum));
		model.addAttribute("pageMaker", new PageDTO(boardService.getNoticeListTotal(), 10, criteria));
		return "admin/adminNoticeModify";
	}

	@PostMapping("/admin/adminNoticeModify")
	public String adminNoticeModify(BoardVO boardVO) {
		boardService.modifyNotice(boardVO);
		return "redirect:adminNoticeView?boNum=" + boardVO.getBoNum();
	}

	// 공지사항 삭제
	@GetMapping("/admin/adminNoticeDelete")
	public String adminNoticeDelete(@RequestParam String boNum) {
		boardService.deleteNotice(boNum);
		return "redirect:adminNoticeList";
	}

	// 1:1 문의 목록
	@GetMapping("/qnaList")
	public String qnaList(HttpServletRequest request, Criteria criteria, Model model) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("qnaList", boardService.getMyQnaList(mbVO.getMbId(), criteria.getPageNum(), criteria.getAmount()));
		model.addAttribute("pageMaker", new PageDTO(boardService.getMyQnaListTotal(mbVO.getMbId()), 10, criteria));
		return "customer/qnaList";
	}

	@GetMapping("/admin/adminQnaList")
	public String adminQnaList(Criteria criteria, Model model) {
		model.addAttribute("qnaList", boardService.getQnaList(criteria));
		model.addAttribute("pageMaker", new PageDTO(boardService.getQnaListTotal(), 10, criteria));
		return "admin/adminQnaList";
	}

	// 1:1 문의 상세
	@GetMapping("/qnaView")
	public String qnaView(@RequestParam String boNum, Model model, Criteria criteria) {
		model.addAttribute("qnaView", boardService.getQnaView(boNum));
		model.addAttribute("pageMaker", new PageDTO(boardService.getQnaListTotal(), 10, criteria));
		model.addAttribute("commentList", boardService.getQnaCommentList(boNum));
		return "customer/qnaView";
	}

	@GetMapping("/admin/adminQnaView")
	public String adminQnaView(@RequestParam String boNum, Model model, Criteria criteria) {
		model.addAttribute("qnaView", boardService.getQnaView(boNum));
		model.addAttribute("pageMaker", new PageDTO(boardService.getQnaListTotal(), 10, criteria));
		return "admin/adminQnaView";
	}

	@ResponseBody
	@RequestMapping(value = "/admin/qnaAnswerList/{boNum}")
	public List<BoardVO> qnaAnswerList(@PathVariable String boNum) {
		return boardService.getQnaCommentList(boNum);
	}	

	// 1:1 문의 등록
	@GetMapping("/qnaWrite")
	public String qnaWrite() {
		return "customer/qnaWrite";
	}

	@PostMapping("/qnaWrite")
	public String qnaWrite(BoardVO boardVO) {
		boardService.insertQna(boardVO);
		return "redirect:qnaList";
	}
	
	// 1:1 문의 답변 등록
	@ResponseBody
	@RequestMapping(value = "/admin/insertQnaAnswer", method = RequestMethod.POST)
	public BoardVO insertQnaAnswer(BoardVO boardVO) {
		int result1 = boardService.modifyQnaState(boardVO);
		if (result1 > 0) {
			int result2 = boardService.insertQnaAnswer(boardVO);
			if (result2 > 0) {
				return boardVO;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	// 1:1 문의 답변 수정
	@ResponseBody
	@RequestMapping(value = "/admin/modifyQnaAnswer", method = RequestMethod.POST)
	public BoardVO modifyQnaAnswer(BoardVO boardVO) {
		int result = boardService.modifyQnaAnswer(boardVO);
		if (result > 0) {
			return boardVO;
		} else {
			return null;
		}
	}

	// 자주 묻는 질문 목록
	@GetMapping("/faqList")
	public String faqList(Model model) {
		model.addAttribute("faqList", boardService.getFaqList());
		return "customer/faqList";
	}
	@GetMapping("/admin/adminFaqList")
	public String adminFaqList(Model model) {
		model.addAttribute("faqList", boardService.getFaqList());
		return "admin/adminFaqList";
	}

	// 자주 묻는 질문 등록
	@ResponseBody
	@RequestMapping(value = "/admin/faqWrite", method = RequestMethod.POST)
	public int faqWrite(BoardVO boardVO) {
		int result = boardService.insertFaq(boardVO);
		return result;
	}
	
	// 자주 묻는 질문 수정
	@ResponseBody
	@RequestMapping(value = "/admin/modifyFaq", method = RequestMethod.POST)
	public int modifyFaq(BoardVO boardVO) {
		int result = boardService.modifyNotice(boardVO);
		return result;
	}
	
	// 자주 묻는 질문 삭제
	@ResponseBody
	@RequestMapping(value = "/admin/deleteFaq", method = RequestMethod.POST)
	public int deleteFaq(@RequestParam String boNum) {
		int result = boardService.deleteNotice(boNum);
		return result;
	}
}