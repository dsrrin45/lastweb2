package com.yedam.app.profile.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.yedam.app.cm.service.CmVO;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.edu.service.EduVO;
import com.yedam.app.member.service.MbVO;
import com.yedam.app.study.service.StudyVO;

@Service
public interface ProfileService {
	// 프로필 확인
	public String intdView(String mbId);
	public List<CmVO> myboardList(String mbId);
	public List<EduVO> myclassList(String mbId);
	// 프로필 수정
	public int intdWrite(MbVO mbVO);
	// 내 게시글 목록
	public List<CmVO> selectAllCmList(String mbId, int pageNum, int amount);
	public int getCmListTotal(String mbId);
	// 내 강의 목록
	public List<EduPersonalClassVO> selectAllClList(String mbId, int pageNum, int amount);
	public int getClListTotal(String mbId);
	
	// 내 chat 목록
	public List<StudyVO> selectmyChatList(String mbId, int pageNum, int amount);
	public int getChatListTotal(String mbId);
}