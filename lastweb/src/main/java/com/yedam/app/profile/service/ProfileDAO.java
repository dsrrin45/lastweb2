package com.yedam.app.profile.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yedam.app.cm.service.CmVO;
import com.yedam.app.profile.mapper.ProfileMapper;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ProfileDAO {
	private final ProfileMapper profileMapper;

	public List<CmVO> selectAllCmList(String mbId, int pageNum, int amount) {
		return profileMapper.selectAllCmList(mbId, pageNum, amount);
	}
}