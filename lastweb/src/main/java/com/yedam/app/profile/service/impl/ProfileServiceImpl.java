package com.yedam.app.profile.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.cm.service.CmVO;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.edu.service.EduVO;
import com.yedam.app.member.service.MbVO;
import com.yedam.app.profile.mapper.ProfileMapper;
import com.yedam.app.profile.service.ProfileService;
import com.yedam.app.study.service.StudyVO;

@Service
public class ProfileServiceImpl implements ProfileService {
	@Autowired
	ProfileMapper profileMapper;

	// 프로필 확인
	@Override
	public String intdView(String mbId) {
		String result = profileMapper.intdView(mbId);
		return result;
	}
	
	@Override
	public List<CmVO> myboardList(String mbId) {
		List<CmVO> result = profileMapper.myboardList(mbId);
		return result;
	}
	
	@Override
	public List<EduVO> myclassList(String mbId) {
		List<EduVO> result = profileMapper.myclassList(mbId);
		return result;
	}

	// 프로필 수정
	@Override
	public int intdWrite(MbVO mbVO) {
		int result = profileMapper.intdWrite(mbVO);
		return result;
	}

	// 내 게시글 목록
	@Override
	public List<CmVO> selectAllCmList(String mbId, int pageNum, int amount) {
		List<CmVO> result = profileMapper.selectAllCmList(mbId, pageNum, amount);
		return result;
	}
	
	@Override
	public int getCmListTotal(String mbId) {
		int result = profileMapper.getCmListTotal(mbId);
		return result;
	}
	
	@Override
	public List<EduPersonalClassVO> selectAllClList(String mbId, int pageNum, int amount) {
		List<EduPersonalClassVO> result = profileMapper.selectAllClList(mbId, pageNum, amount);
		return result;
	}

	@Override
	public int getClListTotal(String mbId) {
		int result = profileMapper.getClListTotal(mbId);
		return result;
	}

	@Override
	public List<StudyVO> selectmyChatList(String mbId, int pageNum, int amount) {
		List<StudyVO> result = profileMapper.selectmyChatList(mbId, pageNum, amount);
		return result;
	}

	@Override
	public int getChatListTotal(String mbId) {
		int result = profileMapper.getChatListTotal(mbId);
		return result;
	}
}