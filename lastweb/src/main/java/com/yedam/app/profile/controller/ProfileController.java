package com.yedam.app.profile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.member.service.MbVO;
import com.yedam.app.profile.service.ProfileService;

@Controller
public class ProfileController {
	@Autowired
	ProfileService profileService;

	// 프로필 조회
	@GetMapping("/profile/{mbId}")
	public String myProfile(@PathVariable("mbId") String mbId, Model model) {
		model.addAttribute("proflieMbId", mbId);
		model.addAttribute("intd", profileService.intdView(mbId));
		model.addAttribute("boardList", profileService.myboardList(mbId));
		model.addAttribute("classList", profileService.myclassList(mbId));
		return "profile/myProfile";
	}

	// 프로필 수정
	@ResponseBody
	@RequestMapping(value = "/intdWrite", method = RequestMethod.POST)
	public int intdWrite(MbVO mbVO) {
		int result = profileService.intdWrite(mbVO);
		return result;
	}	

	@GetMapping("/profile/myBoardList/{mbId}")
	public String myBoardList(@PathVariable("mbId") String mbId, Criteria criteria, Model model) {
		model.addAttribute("proflieMbId", mbId);
		model.addAttribute("communityList", profileService.selectAllCmList(mbId, criteria.getPageNum(), criteria.getAmount()));
		model.addAttribute("pageMaker", new PageDTO(profileService.getCmListTotal(mbId), 10, criteria));
		return "profile/myBoardList";
	}

	@GetMapping("/profile/myClassList/{mbId}")
	public String myClassList(@PathVariable("mbId") String mbId, Criteria criteria, Model model) {
		model.addAttribute("proflieMbId", mbId);
		model.addAttribute("classList", profileService.selectAllClList(mbId, criteria.getPageNum(), criteria.getAmount()));
		model.addAttribute("pageMaker", new PageDTO(profileService.getClListTotal(mbId), 10, criteria));
		return "profile/myClassList";
	}
	
	@GetMapping("/profile/myChatList/{mbId}")
	public String myChatList(@PathVariable("mbId") String mbId, Criteria criteria, Model model) {
		model.addAttribute("proflieMbId", mbId);
		model.addAttribute("myChatList", profileService.selectmyChatList(mbId, criteria.getPageNum(), criteria.getAmount()));
		model.addAttribute("pageMaker", new PageDTO(profileService.getChatListTotal(mbId), 10, criteria));

		return "profile/myChatList";
	}
}