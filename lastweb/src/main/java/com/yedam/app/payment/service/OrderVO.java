package com.yedam.app.payment.service;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class OrderVO {
	List<OrderVO> order;
	
	String orderNum;
	String mbId;
	String rcdName;
	int rcdPost;
	String rcdAddr1;
	String rcdAddr2;
	String rcdTel;
	String request;
	String mileageNum;
	int totalPrice;
	String orderKind;
	String usedMileage;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
	Date orderDate;
	String orderState;
	String trackingNum;
	int goodsCnt;
	
	String orderDetailNum;
	String goodsNum;
	String goodsCount;
	String goodsName;  
	String goodsDef; 
	
	String bookNum;
	String bookTitle;
	String bookWriter;
	int bookPrice;
	int bookStock;
	
	String mbName;
	String mbTel;
	String mbAddress1;
	String mbAddress2;
	String mbEmail;
	int mbMileage;
	
	String clNum;
	String clName;
	
	
}
