package com.yedam.app.payment.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class PaymentVO {
//	card_num  VARCHAR2(30),
//	order_num  VARCHAR2(20),
//	mb_id VARCHAR2(20),
//	pay_state VARCHAR2(20),
//	pay_date date default sysdate
	
	String cardNum;
	String orderNum;
	String mbId;
	String payState;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date payDate;
}
