package com.yedam.app.payment.service;

import lombok.Data;

@Data
public class CartVO {

	String cartNum;
	String mbId;
	String goods;
	int goodsCnt;
	String cartDef;
	
	String bookNum;
	String bookTitle;
	String bookWriter;
	int bookStock;
	int bookPrice;
	
	String clNum;
	String clThumbnail;
	String clName;
	int clAmount;
	

}
