package com.yedam.app.payment.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.member.mapper.MbMapper;
import com.yedam.app.member.service.MbVO;
import com.yedam.app.payment.mapper.CartMapper;
import com.yedam.app.payment.service.CartService;
import com.yedam.app.payment.service.CartVO;
import com.yedam.app.payment.service.MileageVO;
import com.yedam.app.payment.service.OrderDtlVO;
import com.yedam.app.payment.service.OrderVO;
import com.yedam.app.payment.service.StockVO;
import com.yedam.app.payment.service.WishVO;


@Service
public class CartServiceImpl implements CartService{

	@Autowired
	private CartMapper cartMapper;
	
	@Autowired
	private MbMapper mbMapper;
	
	// 찜 추가
	@Override
	public int addWish(WishVO wish) {
		
		return cartMapper.insertWish(wish);
	}
	
	// 찜 해제
	@Override
	public int wishDel(String goodsNum) {
		return cartMapper.deleteWish(goodsNum);
	}
	
	// 찜 확인
	@Override
	public WishVO WishCheck(WishVO wish) {
		 
		return cartMapper.checkWish(wish);
	}
	
	// 찜 목록
	@Override
	public List<WishVO> getWishList(String mbId, Criteria criteria) {
		List<WishVO> wish = cartMapper.getWish(mbId, criteria);
		return wish;
	}
	@Override
	public int getWishListTotal(String mbId) {
		int getBookListTotal = cartMapper.getWishListTotal(mbId);
		return getBookListTotal;
	}
	
	// 장바구니 추가
	@Override
	public int addCart(CartVO cart) {
		//return cartMapper.addCart(cart);
		// 장바구니 데이터 체크
		CartVO checkCart = cartMapper.checkCart(cart);
		
		if(checkCart != null) {
			return 2;
		}
		
		// 장바구니 등록 & 에러시 0 반환
		try {
			return cartMapper.addCart(cart);
		} catch (Exception e) {
			return 0;
		}
	}
	
	// 장바구니 목록
	@Override
	public List<CartVO> getCartList(String mbId) {
		List<CartVO> cart = cartMapper.getCart(mbId);
		return cart;
	}

	// 장바구니 수량 수정
	@Override
	public int modifyCount(CartVO cart) {
		return cartMapper.modifyCount(cart);
	}
	
	// 장바구니 개별 삭제
	@Override
	public int deleteCart(String cartNum) {
		return cartMapper.deleteCart(cartNum);
	}
	
	// 장바구니 선택 삭제
	@Override
	public void delCart(List<String> cartIdxArray) {
		for(int i=0; i<cartIdxArray.size(); i++) {
			cartMapper.deleteCart(cartIdxArray.get(i));
		}
	}

	// 장바구니 결제
	@Override
	public int cartOrder(OrderVO order) {
		return cartMapper.insertOrder(order);

	}

	@Override
	public int cartOrderDtl(List<OrderDtlVO> list) {
		int num = 0;
		for(int i=0; i<list.size(); i++) {
			cartMapper.insertOrderDtl(list.get(i));
			num++;
		}
		return num;
	}
	
	
	@Override
	public int getCartTotal() {
		int getCartTotal = cartMapper.getCartTotal();
		return getCartTotal;
	}
	
	// 상세페이지 즉시구매
	@Override
	public int buyNow(OrderVO order) {
		return cartMapper.insertOrder(order);
	}
	
	
	@Override
	public int insertDtl(OrderDtlVO orderDtl) {
		return cartMapper.insertOrderDtl(orderDtl);
	}

	@Override
	public int updateStock(BookVO book) {
		return cartMapper.updateStock(book);
	}

	@Override
	public int useMileage(MbVO member) {
		return cartMapper.useMileage(member);
	}

	@Override
	public int plusMileage(MileageVO mileage) {
		return cartMapper.insertMileage(mileage);
	}
	
	// 도서 출고 insert
	@Override
	public int outStock(StockVO stock) {
		return cartMapper.outStock(stock);
	}
	
	// 책 출고 내역 insert
	@Override
	public int insertClass(EduPersonalClassVO eduPersonel) {
		return cartMapper.insertClass(eduPersonel);
	}
	
	
	// 주문 내역
	@Override
	public List<OrderVO> getOrdertList(String orderNum) {
		return cartMapper.orderList(orderNum);
	}
	
	@Override
	public List<OrderVO> orderList(Criteria criteria) {
		return cartMapper.payList(criteria);
	}

	@Override
	public int getOrderListTotal() {
		int getOrderListTotal = cartMapper.getOrderListTotal();
		return getOrderListTotal;
	}

	@Override
	public OrderVO getOrder(String orderNum) {
		return cartMapper.orderInfo(orderNum);
	}

	// 마이페이지 주문상세 - 배송정보 수정
	@Override
	public int updateDelivery(OrderVO order) {
		return cartMapper.updateDelivery(order);
	}
	
	@Override
	public List<OrderVO> allOrder(String mbId) {
		return cartMapper.allOrder(mbId);
	}
	
	// 마일리지 내역
	@Override
	public List<MileageVO> mileageList(String mbId) {
		return cartMapper.mileageList(mbId);
		
	}
	

	// 관리자 회원 주문 내역
	@Override
	public List<OrderVO> adminGetOrderList(Criteria criteria) {
		List<OrderVO> orderList = cartMapper.adminGetOrderList(criteria);
		return orderList;
	}

	@Override
	public int adminOrderListTotal() {
		int adminOrderListTotal = cartMapper.adminOrderListTotal();
		return adminOrderListTotal;
	}
	
	// 운송장 등록
	@Override
	public int updateTrack(OrderVO order) {
		return cartMapper.updateTrack(order);
	}











	


}
