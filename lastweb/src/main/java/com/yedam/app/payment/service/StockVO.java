package com.yedam.app.payment.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class StockVO {
	
	String stockNum;
	String bookNum;
	int shippingCnt;

	@DateTimeFormat(pattern="yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
	Date shippingDate;
	String shippingCate;
	String bookTitle;
	String bookStock;
	int stockSum;

}
