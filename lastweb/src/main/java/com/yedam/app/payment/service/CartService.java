package com.yedam.app.payment.service;

import java.util.List;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.member.service.MbVO;

public interface CartService {
	
	// 찜 목록
	public List<WishVO> getWishList(String mbId, Criteria criteria);
	public int getWishListTotal(String mbId);
	
	// 찜 확인
	public WishVO WishCheck(WishVO wish);
	
	// 찜 해제 
	public int wishDel(String goodsNum);
	
	// 찜 등록
	public int addWish(WishVO wish);

	// 장바구니 추가
	public int addCart(CartVO cart);
	
	// 장바구니 목록
	public List<CartVO> getCartList(String mbId);
	
	// 장바구니 수량 수정
	public int modifyCount(CartVO cart);
	
	// 장바구니 삭제
	public int deleteCart(String cartNum);
	
	// 장바구니 선택삭제
	public void delCart(List<String> cartIdxArray);
	
	// 장바구니 수
	public int getCartTotal();

	// 장바구니 결제 
	public int cartOrder(OrderVO order);
	
	public int cartOrderDtl(List<OrderDtlVO> orderDtl);

	
	// 즉시구매
	public int buyNow(OrderVO order);
	
	// 주문상세 테이블 
	public int insertDtl(OrderDtlVO orderDtl);
	
	public int updateStock(BookVO book);
	
	// 멤버 테이블 마일리지
	public int useMileage(MbVO member);
	
	// 마일리지 테이블 업데이트
	public int plusMileage(MileageVO mileage);
	
	// 주문 완료 페이지
	public List<OrderVO> getOrdertList(String orderNum);

	// 마이페이지 주문 목록
	public List<OrderVO> allOrder(String mbId);
	public int getOrderListTotal();
	
	// 마이페이지 주문 상세
	public OrderVO getOrder(String orderNum);
	
	
	// 마이페이지 마일리지 내역
	public List<MileageVO> mileageList(String mbId);
	
	// 책 출고내역 insert
	public int outStock(StockVO stock);
	
	// 배송지정보 변경
	public int updateDelivery(OrderVO order);
	
	// 관리자 회원 주문 내역
	public List<OrderVO> adminGetOrderList(Criteria criteria);
	public int adminOrderListTotal();

	// 관리자 운송장번호 등록
	public int updateTrack(OrderVO order);

	public int insertClass(EduPersonalClassVO eduPersonel);

	public List<OrderVO> orderList(Criteria criteria);
	
	
}
