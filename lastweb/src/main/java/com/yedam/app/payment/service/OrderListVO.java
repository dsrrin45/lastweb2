package com.yedam.app.payment.service;

import java.util.List;

import lombok.Data;
@Data
public class OrderListVO {
	List<OrderListVO> orderList;
	
	int goodsCount;
	String goodsNum;
	int bookPrice;
	int clAmount;
	
	String goodsName;
	String goodsDef;
	
	int shippingFee;
	int goodsTotal;
	int orderTotal;
	
}
