package com.yedam.app.payment.service;

import lombok.Data;

@Data
public class OrderPageItemDTO {
	
	private String bookNum;
	private String clNum;
	
	private String bookTitle;
	private int bookPrice;
	
	
}
