package com.yedam.app.payment.controller;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.yedam.app.book.service.BookService;
import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.edu.service.EduService;
import com.yedam.app.member.service.MbVO;
import com.yedam.app.payment.service.CartService;
import com.yedam.app.payment.service.CartVO;
import com.yedam.app.payment.service.MileageVO;
import com.yedam.app.payment.service.OrderDtlVO;
import com.yedam.app.payment.service.OrderListVO;
import com.yedam.app.payment.service.OrderVO;
import com.yedam.app.payment.service.PaymentVO;
import com.yedam.app.payment.service.StockVO;
import com.yedam.app.payment.service.WishVO;

@Controller
public class PaymentRestController {

	@Autowired
	private CartService cartService;
	
	@Autowired
	private BookService bookService;
	
	@Autowired
	private EduService eduService;


	// 찜 목록
	@GetMapping("/wishBook/{mbId}")
	public String wishlistBook(@PathVariable("mbId") String mbId, Criteria criteria, Model model) {
		model.addAttribute("wishInfo", cartService.getWishList(mbId, criteria));
		model.addAttribute("pageMaker", new PageDTO(cartService.getWishListTotal(mbId), 10, criteria));

		return "/payment/wishlistBook";
	}
	
		
	// 찜 등록
	@PostMapping("/wishBook/add")
	@ResponseBody
	public String addWishPOST(WishVO wish, @RequestParam("goodsNum") String goodsNum, HttpServletRequest request) {
		
		// 로그인 체크
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		
		if (mvo == null) {
			return "3";
		}
		
		String mbId = mvo.getMbId();
		wish.setMbId(mbId);
		wish.setGoodsNum(goodsNum);
		
		WishVO cvo = cartService.WishCheck(wish);
		int result = 0;
		if (cvo == null) {
			 result = cartService.addWish(wish);
		} else {
			 cartService.wishDel(goodsNum);
			 result = 2;
		}
		
		return result + "";
	}	
	

	
	// 찜 삭제
	@PostMapping("/wishBook/delete")
	@ResponseBody
	public String deleteWish(WishVO wish, HttpServletRequest request) {
		cartService.wishDel(wish.getGoodsNum());
		return "";

	}
	
	
	// 도서 장바구니 등록
	@PostMapping("/cart/add")
	@ResponseBody
	public String addCartPOST(CartVO cart, HttpServletRequest request) {
		// 로그인 체크
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo == null) {
			return "5";
		}
		
		// 등록
		String mbId = mvo.getMbId();
		cart.setMbId(mbId);

		int result = cartService.addCart(cart);
		return result + "";
	}
	
	// 강의 장바구니 등록
	@PostMapping("/cart/classAdd")
	@ResponseBody
	public String addClassCartPOST(CartVO cart, HttpServletRequest request) {
		// 로그인 체크
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo == null) {
			return "5";
		}
		
		// 등록
		String mbId = mvo.getMbId();
		cart.setMbId(mbId);

		int result = cartService.addCart(cart);
		return result + "";
	}
	
	// 장바구니 목록
	@GetMapping("/cart/{mbId}")
	public String cartPageGet(@PathVariable("mbId") String mbId, Model model) {
		model.addAttribute("cartInfo", cartService.getCartList(mbId));
		return "/payment/cartList";
	}
	
	// 장바구니 수량 수정
	@PostMapping("/cart/update")
	@ResponseBody
	public String updateCartPost(CartVO cart, HttpServletRequest request) {
		cartService.modifyCount(cart);
		return "redirect:/cart/" + cart.getMbId();
	}

	// 장바구니 개별 삭제
	@PostMapping("/cart/delete")
	@ResponseBody
	public String deleteCartPost(CartVO cart, HttpServletRequest request) {
		cartService.deleteCart(cart.getCartNum());
		return "";
	}
	
	// 장바구니 선택 삭제
	@ResponseBody
	@PostMapping("/deleteCart")
	public List<String> deleteCart(@RequestBody List<String> cartIdxArray) {	
		cartService.delCart(cartIdxArray);
		return cartIdxArray;
	}     
	
	// 장바구니 수
	@GetMapping("/cartCount")
	public String cartCount(Model model) {
		
		model.addAttribute("cart", cartService.getCartTotal());
		return "/layouts/top";
	}
	

	
    
	// 장바구니 결제 페이지
	@RequestMapping(value = "/payment/cartCheckOut", method=RequestMethod.POST)
	public String cartOrderPage(OrderListVO list, Model model, HttpServletRequest request) {
		model.addAttribute("order", list.getOrderList());
		
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			OrderDtlVO orderDtlVO = new OrderDtlVO();
						
			model.addAttribute("orderDtl", orderDtlVO);
			model.addAttribute("member", mvo);
		}
		System.out.println(list);
		return "payment/cartCheckOut";
		
	}
	
	//장바구니 결제
	@PostMapping("/cartOrder")
	@ResponseBody
	public String cartOrder(MileageVO mileage, BookVO book, OrderDtlVO orderDtl, OrderVO order, StockVO stock, EduPersonalClassVO eduPersonel, HttpServletRequest request, RedirectAttributes rttr) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		mvo.setMbMileage(mileage.getMbMileage());
		
		// 주문번호 생성
		Calendar cal = Calendar.getInstance();
	    int year = cal.get(Calendar.YEAR);
	    String ym = year + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1);
	    String ymd = ym +  new DecimalFormat("00").format(cal.get(Calendar.DATE));
	    String subNum = "";
		  
		for(int i = 1; i <= 6; i ++) {
			subNum += (int)(Math.random() * 10);
		}
		String orderNum = ymd + "_" + subNum; //ex) 20230318_373063
		  
		orderDtl.setOrderNum(orderNum);
		order.setOrderNum(orderNum);
		   
		// 등록
		String mbId = mvo.getMbId();
		orderDtl.setMbId(mbId);
		order.setMbId(mbId);
		mileage.setMbId(mbId);
		eduPersonel.setMbId(mbId);
		
		cartService.buyNow(order);
		// 마일리지 사용 
		cartService.useMileage(mvo);
		// 마일리지 내역
		mileage.setMileageDtl("구매 적립");
		cartService.plusMileage(mileage);
		mileage.setMileageDtl("적립금 사용");
		mileage.setMileageChg(mileage.getMileageChg2());
		cartService.plusMileage(mileage);
		
		return orderNum;
	}
	
	

	// 강의 결제 페이지
	@GetMapping("/order")
	public String orderPgae(@RequestParam("goodsCount") int goodsCount, @RequestParam("goodsNum") String goodsNum, 
							@RequestParam("goodsDef") String goodsDef, Model model, HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			
			OrderDtlVO orderDtlVO = new OrderDtlVO();
			orderDtlVO.setGoodsCount(goodsCount);
			orderDtlVO.setGoodsNum(goodsNum);
			orderDtlVO.setGoodsDef(goodsDef);
			
			model.addAttribute("goodsInfo", eduService.getClass(goodsNum));
			model.addAttribute("orderDtl", orderDtlVO);
			model.addAttribute("member", mvo);
			return "payment/classCheckout";
		} else {
			return "redirect:/login";
		}
	}

	
	
	// 강의 즉시구매 
	@PostMapping("/orderDtl")
	@ResponseBody
	public String order(MileageVO mileage, BookVO book, OrderDtlVO orderDtl, OrderVO order, PaymentVO payment, StockVO stock, EduPersonalClassVO eduPersonel, HttpServletRequest request, RedirectAttributes rttr) {
		
		System.out.println(orderDtl);
		// 로그인 체크
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		mvo.setMbMileage(mileage.getMbMileage());
		
//			if (mvo == null) {
//				return "2";
//			}
		
		// 주문번호 생성
		Calendar cal = Calendar.getInstance();
		  int year = cal.get(Calendar.YEAR);
		  String ym = year + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1);
		  String ymd = ym +  new DecimalFormat("00").format(cal.get(Calendar.DATE));
		  String subNum = "";
		  
		  for(int i = 1; i <= 6; i ++) {
		   subNum += (int)(Math.random() * 10);
		  }
		String orderNum = ymd + "_" + subNum; //ex) 20230318_373063
		  
		orderDtl.setOrderNum(orderNum);
		order.setOrderNum(orderNum);
		payment.setOrderNum(orderNum);
		   
		// 등록
		String mbId = mvo.getMbId();
		orderDtl.setMbId(mbId);
		order.setMbId(mbId);
		payment.setMbId(mbId);
		mileage.setMbId(mbId);
		eduPersonel.setMbId(mbId);
	
		cartService.insertDtl(orderDtl);
		cartService.buyNow(order);
		// 마일리지 사용 
		cartService.useMileage(mvo);
		// 마일리지 내역
		mileage.setMileageDtl("구매 적립");
		cartService.plusMileage(mileage);
		mileage.setMileageDtl("적립금 사용");
		mileage.setMileageChg(mileage.getMileageChg2());
		cartService.plusMileage(mileage);
		// 마이페이지 강의 추가
		cartService.insertClass(eduPersonel);
		 

		return orderNum;
	}
	
	
	// 도서 결제 페이지
	@GetMapping("/bookOrder")
	public String orderBook(@RequestParam("goodsCount") int goodsCount, @RequestParam("goodsNum") String goodsNum, 
							@RequestParam("goodsDef") String goodsDef, Model model, HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			
			OrderDtlVO orderDtlVO = new OrderDtlVO();
			orderDtlVO.setGoodsCount(goodsCount);
			orderDtlVO.setGoodsNum(goodsNum);
			orderDtlVO.setGoodsDef(goodsDef);
			
			model.addAttribute("goodsInfo", bookService.getBook(goodsNum));
			model.addAttribute("orderDtl", orderDtlVO);
			model.addAttribute("member", mvo);
			return "payment/bookCheckout";
		} else {
			return "redirect:/login";
		}
	}

	
	
	// 강의 즉시구매 
	@PostMapping("/orderBook")
	@ResponseBody
	public String orderBook(MileageVO mileage, BookVO book, OrderDtlVO orderDtl, OrderVO order, PaymentVO payment, StockVO stock, EduPersonalClassVO eduPersonel, HttpServletRequest request, RedirectAttributes rttr) {
		
		System.out.println(orderDtl);
		// 로그인 체크
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		mvo.setMbMileage(mileage.getMbMileage());
		
//			if (mvo == null) {
//				return "2";
//			}
		
		// 주문번호 생성
		Calendar cal = Calendar.getInstance();
		  int year = cal.get(Calendar.YEAR);
		  String ym = year + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1);
		  String ymd = ym +  new DecimalFormat("00").format(cal.get(Calendar.DATE));
		  String subNum = "";
		  
		  for(int i = 1; i <= 6; i ++) {
		   subNum += (int)(Math.random() * 10);
		  }
		String orderNum = ymd + "_" + subNum; //ex) 20230318_373063
		  
		orderDtl.setOrderNum(orderNum);
		order.setOrderNum(orderNum);
		payment.setOrderNum(orderNum);
		   
		// 등록
		String mbId = mvo.getMbId();
		orderDtl.setMbId(mbId);
		order.setMbId(mbId);
		payment.setMbId(mbId);
		mileage.setMbId(mbId);
		eduPersonel.setMbId(mbId);
	
		cartService.insertDtl(orderDtl);
		cartService.buyNow(order);
		// 책재고 update
		cartService.updateStock(book);	
		// 마일리지 사용 
		cartService.useMileage(mvo);
		// 마일리지 내역
		mileage.setMileageDtl("구매 적립");
		cartService.plusMileage(mileage);
		mileage.setMileageDtl("적립금 사용");
		mileage.setMileageChg(mileage.getMileageChg2());
		cartService.plusMileage(mileage);
		// 책 출고 내역 insert
		cartService.outStock(stock);
		 

		return orderNum;
	}

	// 결제완료페이지
	@GetMapping("/checkoutSuccess/{orderNum}")
	public String checkoutSuccess(@PathVariable("orderNum") String orderNum,  Model model, OrderVO order, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		order.setOrderNum(orderNum);
		model.addAttribute("member", mvo);
		model.addAttribute("orderInfo", cartService.getOrdertList(orderNum));
		return "/payment/checkoutSuccess";
	}
	
	
	// 마이페이지 마일리지 내역
	@GetMapping("/mypage/mileage")
	public String myMileage(MileageVO mileage, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		String mbId = mvo.getMbId();
		mileage.setMbMileage(mileage.getMbMileage());
		model.addAttribute("mileageList", cartService.mileageList(mbId));
		return "mypage/mypageMyMileage";
	}
	
	// 마이페이지 주문 내역
	@GetMapping("/mypage/orderList")
	public String mypageOrderList(Model model, HttpServletRequest request, Criteria criteria) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		String mbId = mvo.getMbId();
		model.addAttribute("member", mvo);
		//model.addAttribute("orderList", cartService.allOrder(mbId));
		model.addAttribute("orderList", cartService.adminGetOrderList(criteria));
		model.addAttribute("pageMaker", new PageDTO(cartService.getOrderListTotal(), 10, criteria));
		return "mypage/mypageMyOrdersList";
	}
	
	// 마이페이지 주문 상세
	@GetMapping("mypage/myOrder/{orderNum}")
	public String bookInfoList(@PathVariable("orderNum") String orderNum, Model model) {
		model.addAttribute("order", cartService.getOrder(orderNum));

		return "/mypage/mypageMyOrdersView";
	}
	
	// 마이페이지 주문상세 - 배송정보 수정
	@PostMapping("mypage/deliveryMod")
	public String deliveryMod(OrderVO order) {
		cartService.updateDelivery(order);
		return "mypage/mypageMyOrdersList"; 
	}


	// 관리자 - 회원 주문 목록
	@GetMapping("/admin/shoppingList")
	public String adminShoppingList(Criteria criteria, Model model) {
		model.addAttribute("orderList", cartService.adminGetOrderList(criteria));
		model.addAttribute("pageMaker", new PageDTO(cartService.getOrderListTotal(), 10, criteria));
		return "/admin/adminShoppingList";
	}
	
	// 관리자 - 회원 주문 상세보기
	@GetMapping("/admin/shoppingView")
	public String adminShoppingView(@RequestParam String orderNum, Model model, Criteria criteria) {
		model.addAttribute("order", cartService.getOrder(orderNum));
		model.addAttribute("pageMaker", new PageDTO(cartService.getOrderListTotal(), 10, criteria));
		return "/admin/adminShoppingView";
	}
	
	// 관리자 - 운송장번호 등록
	@PostMapping("/admin/updateTrack")
	public String updateTrack(OrderVO order, Criteria criteria, Model model) {
		cartService.updateTrack(order);
		model.addAttribute("pageMaker", new PageDTO(cartService.getOrderListTotal(), 10, criteria));

		return "/admin/adminShoppingList";
	}
	

}
