package com.yedam.app.payment.mapper;

import java.util.List;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.member.service.MbVO;
import com.yedam.app.payment.service.CartVO;
import com.yedam.app.payment.service.MileageVO;
import com.yedam.app.payment.service.OrderDtlVO;
import com.yedam.app.payment.service.OrderVO;
import com.yedam.app.payment.service.StockVO;
import com.yedam.app.payment.service.WishVO;

public interface CartMapper {

	
	// 카트 추가
	public int addCart(CartVO cart);
	
	// 카트 삭제
	public int deleteCart(String cartNum);
	
	// 카트 선택삭제
	public int delCart(String cartIdxArray);
	
	// 카트 수량 수정
	public int modifyCount(CartVO cart);
	
	// 카트 목록
	public List<CartVO> getCart(String mbId);
	
	// 카트 확인
	public CartVO checkCart(CartVO cart);
	
	// 카트 수
	public int getCartTotal();
	
	
	// 찜 목록
	public List<WishVO> getWish(String mbId, Criteria criteria);
	public int getWishListTotal(String mbId);
	
	// 찜 확인
	public WishVO checkWish(WishVO wish);

	// 찜 등록
	public int insertWish(WishVO wish);
		
	// 찜 해제
	public int deleteWish(String goodsNum);
	
	// 마이페이지 내강의 등록
	public int insertClass(EduPersonalClassVO eduPersonel);

	
	// 구매
	public int insertOrder(OrderVO order);
	public int insertOrderDtl(OrderDtlVO orderDtl);
	
	// 주문 목록
	public List<OrderVO> orderList(String orderNum);

	// 책 재고 update
	public int updateStock(BookVO book);
	
	// 마일리지 사용
	public int useMileage(MbVO member);
	
	// 마이페이지 마일리지 내역 
	public int insertMileage(MileageVO mileage);
	
	// 마이페이지 주문 내역
	public List<OrderVO> allOrder(String mbId);
	
	// 마이페이지 주문 내역 상세
	public OrderVO orderInfo(String orderNum);
	
	// 마이페이지 마일리지 내역
	public List<MileageVO> mileageList(String mbId);
	
	// 책 출고내역 insert
	public int outStock(StockVO stock);
	
	// 마이페이지 주문상세 - 배송정보 수정
	public int updateDelivery(OrderVO order);
	
	// 관리자 회원 주문 내역
	public List<OrderVO> adminGetOrderList(Criteria criteria);
	public int adminOrderListTotal();
	public int getOrderListTotal();

	// 관리자 운송장 번호 등록
	public int updateTrack(OrderVO order);
	
	public List<OrderVO> payList(Criteria criteria);
}
