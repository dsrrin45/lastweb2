package com.yedam.app.payment.service;

import lombok.Data;

@Data
public class WishVO {
	String wishNum;
	String mbId;
	String goodsNum;
	String wishState;
	String wishDef;
	
	String bookNum;
	String bookTitle;
	String bookWriter;
	String bookPrice;
	int bookStock;
	int bookRating;
	
}
