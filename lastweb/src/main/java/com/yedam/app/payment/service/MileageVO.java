package com.yedam.app.payment.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class MileageVO {
	String mileageNum;
	String mbId;
	int mileageChg;
	int mileageChg2;
	String mileageDtl;
	@DateTimeFormat(pattern="yyyy-MM-dd")
	Date mileageDate;
	int mbMileage;
	int mileageSum;
}
