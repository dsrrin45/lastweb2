package com.yedam.app.payment.service;

import lombok.Data;

@Data
public class OrderDtlVO {
//	order_detail_num VARCHAR2(20) PRIMARY Key,
//	order_num VARCHAR2(20),
//	goods_num VARCHAR2(20),
//	goods_count number(2,0),
//	goods_def char(1)
	
	String orderDetailNum;
	String orderNum;
	String goodsNum;
	String mbId;
	int goodsCount;
	String goodsDef;
	String goodsName;
	


	
}
