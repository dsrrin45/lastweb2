package com.yedam.app.config.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.mapper.MainMapper;
import com.yedam.app.config.service.MainService;
import com.yedam.app.edu.service.EduVO;


@Service
public class MainServiceImpl implements MainService {

	@Autowired
	MainMapper mainMapper;
	
	@Override
	public List<EduVO> eduRecList() {
		List<EduVO> result = mainMapper.eduRecList();
		return result;
	}

	@Override
	public List<BookVO> bookRecList() {
		List<BookVO> result = mainMapper.bookRecList();
		return result;
	}

}
