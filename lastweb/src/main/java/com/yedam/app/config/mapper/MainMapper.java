package com.yedam.app.config.mapper;

import java.util.List;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.edu.service.EduVO;

public interface MainMapper {

	public List<EduVO> eduRecList();

	public List<BookVO> bookRecList();

}
