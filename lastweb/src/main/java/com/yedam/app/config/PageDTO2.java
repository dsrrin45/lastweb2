package com.yedam.app.config;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class PageDTO2 {
	private int pageCount;
	private int startPage;
	private int endPage;
	private int realEnd;
	private boolean prev, next;
	private int total;
	private Criteria2 criteria2;

	public PageDTO2() {
	};

	public PageDTO2(int total, int pageCount, Criteria2 criteria2) {
		this.total = total;
		this.criteria2 = criteria2;
		this.pageCount = pageCount;

		this.endPage = (int) (Math.ceil(criteria2.getPageNum() * 1.0 / pageCount)) * pageCount;
		this.startPage = endPage - (pageCount - 1);

		realEnd = (int) (Math.ceil(total * 1.0 / criteria2.getAmount()));

		if (endPage > realEnd) {
			endPage = realEnd == 0 ? 1 : realEnd;
		}
		prev = startPage > 1;
		next = endPage < realEnd;
	}
}
