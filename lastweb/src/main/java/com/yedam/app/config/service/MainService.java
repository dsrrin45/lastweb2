package com.yedam.app.config.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.edu.service.EduVO;

@Service
public interface MainService {

	public List<EduVO> eduRecList();

	public List<BookVO> bookRecList();

}
