package com.yedam.app.config.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.yedam.app.config.service.MainService;
import com.yedam.app.study.service.StudyService;

@Controller
public class MainController {
	@Autowired
	StudyService studyService;
	@Autowired
	MainService mainService;
	
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("bigCateList", studyService.bigCateList());
		model.addAttribute("eduList", mainService.eduRecList());
		model.addAttribute("bookList", mainService.bookRecList());
		
		return "index";
	}
}
