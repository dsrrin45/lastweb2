package com.yedam.app.study.controller;




import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.cm.service.CmVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.study.service.StudyService;
import com.yedam.app.study.service.StudyVO;


@Controller
//@RestController
public class StudyController {

	@Autowired
	StudyService studyService;
	
	@GetMapping("/study")
	public String electAllStudyList(Criteria criteria, Model model) {
		model.addAttribute("studyList", studyService.selectAllStudyList(criteria));
		model.addAttribute("pageMaker", new PageDTO(studyService.getStudyListTotal(), 10, criteria));
		//model.addAttribute("cateList", studyService.cateList());
		return "study/studyList";
	}
	
	// 스터디 목록 카테고리 검색
	@GetMapping("/studyCate")
	public String catesearch(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Criteria criteria, Model model) {
	    model.addAttribute("studyList", studyService.searchCate(keyword, bigkeyword, criteria));
	    model.addAttribute("pageMaker", new PageDTO(studyService.getCateTotal(keyword, bigkeyword), 10, criteria)); 
	    return "study/studyList";
	} 
	
	
	
	
	
	
	// 등록화면 페이지 호출
		@GetMapping("/studyWrite")
		public String inputStudyForm(Criteria criteria, Model model) {
			model.addAttribute("bigCateList", studyService.bigCateList());
			model.addAttribute("cateList", studyService.cateList());
			model.addAttribute("pageMaker", new PageDTO(studyService.getStudyListTotal(), 10, criteria));
			return "study/studyWrite";
		}

		// 스터디글 등록 후 처리 
		@ResponseBody
		@RequestMapping(value = "/insertStudy", method = RequestMethod.POST)
		public int insertStudy(StudyVO studyVO) {
			//스터디글 등록
			int result1 = studyService.insertStudy(studyVO);
			
			return result1;
		}		
		
		
		// 스터디 참석자 등록 
		@ResponseBody
		@RequestMapping(value = "/insertChatter", method = RequestMethod.POST)
		public int insertChatter(StudyVO studyVO) {
			//참석자 등록
			int result1 = studyService.addChatter(studyVO);
			
			return result1;
		}
		

		
	//스터디 상세페이지
		@GetMapping("/studyView/{studyNum}")
		public String studyInfo(@PathVariable("studyNum") String studyNum, Model model, Criteria criteria){
			model.addAttribute("study", studyService.selectStudyInfo(studyNum));
			model.addAttribute("pageMaker", new PageDTO(studyService.getStudyListTotal(), 10, criteria));
//			model.addAttribute("reply", cmService.selectCmReply(comNum));
			return "study/studyView";
		}
		
		
		//스터디 수정페이지 호출
		@GetMapping("/studyMod/{studyNum}")
		public String studyModInfo(@PathVariable("studyNum") String studyNum, Model model){
			model.addAttribute("cateList", studyService.cateList());
			model.addAttribute("bigCateList", studyService.bigCateList());
			model.addAttribute("study", studyService.selectStudyInfo(studyNum));
			return "study/studyMod";
		}
		
		// 스터디 수정 처리
		@PostMapping("/studyModify")
		@ResponseBody
		public int modifyCm(StudyVO studyVO) {
			//model.addAttribute("cm", cmService.selectCmInfo(comNum));
			int result = studyService.modifyStudy(studyVO);
			return result;
//			return "redirect:studyView/" + studyVO.getStudyNum();
//			return "redirect:/study";
		} 
		
		
//		@GetMapping("/studyView")
//		public String studyInfo(@RequestParam String studyNum, Model model){
//			model.addAttribute("study", studyService.selectStudyInfo(studyNum));
//			//model.addAttribute("pageMaker", new PageDTO(studyService.getStudyListTotal(), 10, criteria));
//			return "study/studyView";
//		}
		

	// 스터디 게시글 삭제 
		@GetMapping("/studyDelete/{studyNum}")
		public String studyDelete(@PathVariable("studyNum") String studyNum) {
			studyService.deleteStudy(studyNum);
			return "redirect:/study";
		}
		
	// 
//		// 스터디 방 페이지 호출
//		@GetMapping("/myChatView")
//		public String myChatView() {	
//			return "profile/myChatView";
//		}
//	
}
