package com.yedam.app.study.mapper;


import java.util.List;

import com.yedam.app.cm.service.CmVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.study.service.BigCateVO;
import com.yedam.app.study.service.CateVO;
import com.yedam.app.study.service.StudyVO;


public interface StudyMapper {
	// 조회
	public List<StudyVO> selectAllStudyList(Criteria criteria);
	public int getStudyListTotal();
	
	// 스터디 목록 카테고리 검색
	public List<StudyVO> searchCate(String bigkeyword, String keyword, Criteria criteria);
	public int getCateTotal(String keyword, String bigkeyword);
	
	//상세조회
	public StudyVO selectStudyInfo(String studyNum);
	public int insertStudyInfo(StudyVO studyVO);
	
	// 스터디 게시글 등록
	public int insertStudy(StudyVO studyVO);
	
	//스터디 게시글 수정
	public int modifyStudy(StudyVO studyVO);
		
	//스터디 삭제
	public int deleteStudy(String studyNum);
	
	// 스터디 삭제시 해당 스터디 참석자 삭제
	public int delChatter(String studyNum);
	
	//카테고리 리스트 대대
	public List<BigCateVO> bigCateList();
	
	//카테고리 리스트 소
	public List<CateVO> cateList();
	
	// 방금 등록한 스터디 조회
	public StudyVO selectNewStudy();
	
	//스터디 참석자(방장) 등록
	public int addHostChatter(StudyVO studyVO);
	
	//스터디 참석자 등록
	public int addChatter(StudyVO studyVO);
	
	//스터디 참석자 수 변경
	public int updateChatter(StudyVO studyVO);
	
	//스터디 모집 상태 변경
	public int updateRecruitState(StudyVO studyVO);
	
	
}
