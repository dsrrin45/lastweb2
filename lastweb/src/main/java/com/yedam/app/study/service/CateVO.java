package com.yedam.app.study.service;



import lombok.Data;

@Data
public class CateVO {
	//카테고리 넘버
	String scaNum;
	//카테고리 이름
	String scaName;
	// 상위 카테고리
	String bcaNum;
	
	//상위 카테고리 이름
	String bcaName;


	
}
