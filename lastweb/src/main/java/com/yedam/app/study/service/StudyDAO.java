package com.yedam.app.study.service;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.yedam.app.config.Criteria;
import com.yedam.app.study.mapper.StudyMapper;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class StudyDAO {
	private final StudyMapper studyMapper;
	
	public List<StudyVO> getNoticeList(Criteria cri) {
		return studyMapper.selectAllStudyList(cri);
	}
	
	
}
