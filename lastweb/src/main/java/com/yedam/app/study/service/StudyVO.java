package com.yedam.app.study.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class StudyVO {
	String studyNum;
	String bcaNum;
	String scaNum;
	String studyTitle;
	String studyContent;
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	Date studyWriteDate;
	int studyNow;
	int studyMax;
	String recruitState;
	String mbId;
	String reportNum;
	String mbNick;
	
	String studyChatterNum;
	String studyPower;
	String mbProfile;
//
}
