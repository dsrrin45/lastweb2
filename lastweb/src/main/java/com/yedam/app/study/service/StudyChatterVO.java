package com.yedam.app.study.service;

import lombok.Data;

@Data
public class StudyChatterVO {
	String studyChatterNum;
	String studyNum;
	String studyPower;
	String mbId;

}
