package com.yedam.app.study.service;

import java.util.List;

import com.yedam.app.cm.service.CmVO;
import com.yedam.app.config.Criteria;

public interface StudyService {
	// 스터디 전체 목록 조회
	public List<StudyVO> selectAllStudyList(Criteria criteria);
	public int getStudyListTotal();
	
	// 스터디 목록 카테고리 검색
	public List<StudyVO> searchCate(String bigkeyword, String keyword, Criteria criteria);
	public int getCateTotal(String keyword, String bigkeyword);
	
	//상세
	public StudyVO selectStudyInfo(String studyNum);
	public int insertStudyInfo(StudyVO studyVO);
	
	// 스터디 게시글 등록
	public int insertStudy(StudyVO studyVO);
	
	//스터디 게시글 삭제(수정)
	public int deleteStudy(String studyNum);
	
	//스터디 게시글 수정
	public int modifyStudy(StudyVO studyVO);
	
	
	//카테고리 리스트 대대
	public List<BigCateVO> bigCateList();
	
	//카테고리 리스트
	public List<CateVO> cateList();
	
	// 방금 등록한 스터디 조회
	public StudyVO selectNewStudy();
	
	
	
	//스터디 참석자 등록
	public int addChatter(StudyVO studyVO);
	
	
}
