package com.yedam.app.study.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.config.Criteria;
import com.yedam.app.study.mapper.StudyMapper;
import com.yedam.app.study.service.BigCateVO;
import com.yedam.app.study.service.CateVO;
import com.yedam.app.study.service.StudyService;
import com.yedam.app.study.service.StudyVO;

@Service
public class StudyServiceImpl implements StudyService {

	@Autowired
	StudyMapper studyMapper;
	
	@Override
	public List<StudyVO> selectAllStudyList(Criteria cri) {
		List<StudyVO> studyList = studyMapper.selectAllStudyList(cri);
		return studyList;
	}
	
	@Override
	public int getStudyListTotal() {
		int getNoticeListTotal = studyMapper.getStudyListTotal();
		return getNoticeListTotal;
	}

	// 상세조회
	@Override
	public StudyVO selectStudyInfo(String studyNum) {
		return studyMapper.selectStudyInfo(studyNum);
	}

	@Override
	public int insertStudyInfo(StudyVO studyVO) {
		return 0;
	}

	// 카테고리 리스트
	@Override
	public List<CateVO> cateList() {
		System.out.println("cateList...");
		return studyMapper.cateList();
	}

	@Override
	public List<BigCateVO> bigCateList() {
		return studyMapper.bigCateList();
	}

	// 스터디 게시글 삭제
	@Override
	public int deleteStudy(String studyNum) {
		
		//스터디 게시글 삭제처리
		int result = studyMapper.deleteStudy(studyNum);
		if(result > 0) {
			// 해당 스터디 참석자 삭제 처리
			studyMapper.delChatter(studyNum);
		}
		return result;
	}

	// 스터디 게시글 등록
	@Override
	public int insertStudy(StudyVO studyVO) {

		int result = studyMapper.insertStudy(studyVO);
		if (result > 0) {
			studyMapper.addHostChatter(studyVO);
			studyMapper.updateChatter(studyVO); 
		} 
		return result;
	}

	@Override
	public StudyVO selectNewStudy() {
		return studyMapper.selectNewStudy();
	}



	@Override
	public int addChatter(StudyVO studyVO) {
		int result;
		
		//참가자 등록
		result = studyMapper.addChatter(studyVO);
		if(result == 0 ) 
			return result;
		
		// study_board 테이블에 참석자 수를 변경
		studyMapper.updateChatter(studyVO); 
		
		// study_board에 상태 변경
		studyMapper.updateRecruitState(studyVO);
		
		return result;
	}

	//스터디 게시글 수정
	@Override
	public int modifyStudy(StudyVO studyVO) {
		int result;

		// 모집 내용 변경
		result = studyMapper.modifyStudy(studyVO);

		// study_board에 상태 변경
		studyMapper.updateRecruitState(studyVO);

		return result;
	}

	@Override
	public List<StudyVO> searchCate(String bigkeyword, String keyword, Criteria criteria) {
		return studyMapper.searchCate(bigkeyword, keyword, criteria);
	}

	@Override
	public int getCateTotal(String keyword, String bigkeyword) {
		return studyMapper.getCateTotal(keyword, bigkeyword);
	}

}
