package com.yedam.app.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.yedam.app.member.service.MbService;
import com.yedam.app.member.service.MbVO;

public class CustomLoginSuccessHandler implements AuthenticationSuccessHandler {
	@Autowired
	private MbService mbService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		HttpSession session = request.getSession();
		
		if(session != null) {
			MbVO mbVO = new MbVO();
			mbVO.setMbId(authentication.getName());
			MbVO member = (MbVO)session.getAttribute("login");
			if(member == null) {
				member = mbService.login(mbVO);
				session.setAttribute("login", member);
			}
		}	
		response.sendRedirect("/boot/");
	}
}
