package com.yedam.app.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {
//	@Bean
//	public PasswordEncoder passwordEncoder() {
//		return new BCryptPasswordEncoder();
//	}

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http
			.authorizeHttpRequests((requests) -> requests
				.antMatchers("/", "/login", "/join", "/findId", "/findPw", "/css/**", "/font/**", "/img/**", "/js/**", "/video/**").permitAll()
				.antMatchers("/mypage/**").hasRole("MEMBER")
				.antMatchers("/teacher/**").hasRole("TEACHER")
				.antMatchers("/admin/**").hasRole("ADMIN")
//				.anyRequest().authenticated()
			)
			.formLogin((form) -> form
				.loginPage("/login")
				.defaultSuccessUrl("/")
				.permitAll()
				.usernameParameter("mbId")
				.passwordParameter("mbPw")
				.successHandler(successhandler())
			)
			.logout((logout) -> logout.logoutSuccessUrl("/login").permitAll())
			.csrf().disable()
			.exceptionHandling()
				.accessDeniedPage("/accessDenied");

		return http.build();
	}
	
	@Bean
	public CustomLoginSuccessHandler successhandler() {
		return new CustomLoginSuccessHandler();
	}
}