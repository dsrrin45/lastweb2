package com.yedam.app.exam.mapper;

import java.util.List;

import com.yedam.app.exam.service.QuestionVO;

public interface ExamMapper {

	public List<QuestionVO> questionAllList(int teNum);
	
	public List<QuestionVO> getSub(int teNum);
	
	public int updateQues (QuestionVO questionVO);
	
	public int insertQues(QuestionVO questionVO);
	
	public List<QuestionVO> quizList(int teNum);
	
}
