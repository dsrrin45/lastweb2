package com.yedam.app.exam.service;

import lombok.Data;

@Data
public class QuestionVO {
	int questionNum;
	String question;
	String questionSub;
	String vi1;
	String vi2;
	String vi3;
	String vi4;
	int answer;
	String commetary;
	int teNum;
	int teYear;
	int teRound;
	int teTime;
	String sca_num;
	int quesNum;
}
