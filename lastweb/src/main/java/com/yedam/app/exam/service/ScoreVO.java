package com.yedam.app.exam.service;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class ScoreVO {
	int myscoreNum;
	int teNum;
	int myscore;
	Date myDate;
	int teYear;
	int teRound;
	String mbId;
	String bcaNum;
	String scaNum;
	String questionSub;
	List<WrongVO> wrongList;
	int score;
	int teTime;
	String testCheck;
}
