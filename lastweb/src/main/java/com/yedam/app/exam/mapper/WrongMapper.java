package com.yedam.app.exam.mapper;

import java.util.List;

import com.yedam.app.exam.service.WrongVO;

public interface WrongMapper {
	public int insertWrong(WrongVO wrongVO);	
	
	public List<WrongVO> selectWrongList(int myscoreNum);
	
	public List<WrongVO> selectScoring(int myscoreNum);
	
	public int updateWrong(WrongVO wrongVO);
}
