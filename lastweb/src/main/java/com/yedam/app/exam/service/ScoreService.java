package com.yedam.app.exam.service;

import java.util.List;

import com.yedam.app.config.Criteria;

public interface ScoreService {

	public int insertScore(ScoreVO scoreVO);
	
	public ScoreVO selectScore(int myscoreNum);
	
	public List<ScoreVO> selectNote(String mbId,int pageNum, int amount);
	
	public int getNoteListTotal(String mbId);
	
	public List<WrongVO> selectWrongList(int myscoreNum);
	
	public List<WrongVO> selectScoring(int myscoreNum);
	
	public List<ScoreVO> getScore(ScoreVO scoreVO);
	
	public int updateWrong(List<WrongVO> wrongVO);
	
	public int selNoteCate(String bigkeyword, String keyword, String mbId);
	
	public List<ScoreVO> noteCateList(String bigkeyword, String keyword, Criteria criteria ,String mbId);
}
