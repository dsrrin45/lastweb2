package com.yedam.app.exam.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.exam.service.ExamService;
import com.yedam.app.exam.service.QuestionVO;
import com.yedam.app.exam.service.ScoreService;
import com.yedam.app.exam.service.ScoreVO;
import com.yedam.app.exam.service.TestVO;
import com.yedam.app.exam.service.WrongVO;
import com.yedam.app.member.service.MbVO;

@Controller
//@RestController // restcontroller는 페이지를 반환하지 않음
public class ExamController {
	//
	
	@Autowired
	ExamService examService;
	
	@Autowired
	ScoreService scoreService;
		
	@GetMapping("/test")
	public String test() {
		return "test";
	}

	//문제목록
	@GetMapping("/examList")
	public String examList(Model model) {
		List<TestVO> test = examService.examList();
		model.addAttribute("exam", test);
		return "exam/examList";
	}
	
	@GetMapping("/examYear")
	@ResponseBody
	public List<TestVO> examRound(@RequestParam("teYear") int teYear) {
		List<TestVO> round = examService.examRound(teYear);
		System.out.println("==================================teYear : " + teYear);
		System.out.println("==================================round : " + round);		
		
		return round;
	}
	
	@GetMapping("/examCate")
	public String examList(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Model model) {
		model.addAttribute("pageMaker", examService.selCateTotal(bigkeyword, keyword));
		List<TestVO> testVO = examService.examTest(bigkeyword, keyword);
		model.addAttribute("exam", testVO);
		return "exam/examList";
	}
	
	
	//오답노트목록
	@GetMapping("/examNoteList/{mbId}")
	public String examNoteList(@PathVariable String mbId, Model model, Criteria criteria,HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		model.addAttribute("examNoteList", scoreService.selectNote(mvo.getMbId(), criteria.getPageNum(), criteria.getAmount()));
		model.addAttribute("pageMaker", new PageDTO(scoreService.getNoteListTotal(mvo.getMbId()), 10, criteria));
		System.out.println(mbId);
		return "exam/examNoteList";
	}
	
	@GetMapping("/noteCate/{mbId}")
	public String noteCateSer(@PathVariable String mbId,@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword,Criteria criteria, Model model,HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		model.addAttribute("examNoteList", scoreService.noteCateList(bigkeyword, keyword, criteria, mvo.getMbId()));
		model.addAttribute("pageMaker", new PageDTO(scoreService.selNoteCate(bigkeyword, keyword,mvo.getMbId()), 10, criteria));
		return "exam/examNoteList";
	}
	
	//오답노트 상세
	@GetMapping("/examNoteView/{myscoreNum}/{teNum}")
	public String examNoteView(@PathVariable int myscoreNum,@PathVariable int teNum, Model model,HttpServletRequest request, ScoreVO scoreVO) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		List<WrongVO> wrongVO = scoreService.selectWrongList(myscoreNum);
		model.addAttribute("member", mvo);
		model.addAttribute("examNoteView", wrongVO);
		model.addAttribute("get",  scoreService.getScore(scoreVO));
		model.addAttribute("getTest", examService.getTest(teNum));
		return "exam/examNoteView";
	}
	
	//오답노트 수정
	@PostMapping("/updateWrong")
	@ResponseBody
	public int updateWrong(@RequestBody List<WrongVO> wrongVO) {
		System.out.println(wrongVO);
		int result = scoreService.updateWrong(wrongVO);
		return result;
	}
	
	//시험결과
	@GetMapping("/examResult")
	public String getScore(@ModelAttribute ScoreVO scoreVO, Model model) {
			System.out.println(scoreVO);
			model.addAttribute("examResult", scoreService.selectScore(scoreVO.getMyscoreNum()));
			model.addAttribute("get", scoreService.getScore(scoreVO));
			return "exam/examResult";
	}
	
	@PostMapping("/examResult")
	@ResponseBody
	public int scoreInsert(@RequestBody ScoreVO scoreVO, Model model) {
		scoreService.insertScore(scoreVO);
		System.out.println(scoreVO);
		return scoreVO.getMyscoreNum();
	}
	
	//채점결과
	@GetMapping("/examResultView/{myscoreNum}/{teNum}")
	public String examResultView(@PathVariable int myscoreNum, @PathVariable int teNum, Model model,HttpServletRequest request, ScoreVO scoerVO) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		model.addAttribute("getTest", examService.getTest(teNum));
		model.addAttribute("member", mvo);
		model.addAttribute("examResultView", scoreService.selectScoring(myscoreNum));
		return "exam/examResultView";
	}
	
	//문제상세
	@GetMapping("/examView/{teNum}")
	public String QuestionList(@PathVariable int teNum, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if(mvo != null) {
			model.addAttribute("member", mvo);
			model.addAttribute("examView", examService.questionList(teNum));
			model.addAttribute("test", examService.getTest(teNum));
			return "exam/examView";
		}else {
			return "redirect:/login";
		}
	}
	//관리자 문제 목록
	@GetMapping("/admin/adminExamList")
	public String adminExamList(Criteria criteria, Model model){
		model.addAttribute("adminExamList", examService.selectTest(criteria));
		model.addAttribute("pageMaker", new PageDTO(examService.getTestTotal(), 10, criteria ));
		return "admin/adminExamList";
	}
	
	@GetMapping("/admin/serCate")
	public String searchCate(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword,Criteria criteria, Model model) {
		model.addAttribute("adminExamList", examService.testList( bigkeyword, keyword, criteria));
		model.addAttribute("pageMaker", new PageDTO(examService.selCateTotal(bigkeyword, keyword), 10, criteria));
		return "admin/adminExamList";
	}
	
	//문제 상세 페이지 이동
	@GetMapping("/admin/adminExamView/{teNum}")
	public String info(@PathVariable int teNum, Model model) {
		model.addAttribute("get", examService.getTest(teNum));
		model.addAttribute("adminExamView", examService.questionList(teNum));
		return "admin/adminExamView";
	}
	
	//모의고사 등록
	@GetMapping("/admin/adminExamTest")
	public String adminExamTest(Model model) {
		model.addAttribute("cateList", examService.cateList());
		model.addAttribute("bigCateList", examService.bigCateList());
		return "admin/adminExamTest";
	}
	
	@PostMapping("/admin/adminExamTest")
	public String insertTest(TestVO testvo) {
		examService.insertTest(testvo);
		return "redirect:/admin/adminExamList";
	}
	
	//문제 수정
	@PostMapping("/admin/adminExamUp/{teNum}")
	public String updateExam(@PathVariable int teNum, QuestionVO questionvo) {
		examService.updateQues(questionvo);
		return "redirect:/admin/adminExamView/"+teNum;
	}
	
	//문제 삭제
	@GetMapping("/admin/deleteExam/{teNum}")
	public String deleteTest(@PathVariable int teNum) {
		examService.daleteTest(teNum);
		System.out.println(teNum);
		return "redirect:/admin/adminExamList";
	}
	
	//문제 등록
	@GetMapping("/admin/adminExamForm/{teNum}")
	public String adminExamForm(@PathVariable int teNum,Model model) {
		model.addAttribute("adminExamForm", teNum);
		return "admin/adminExamForm";
	}
		
	@PostMapping("/admin/adminExamForm/{teNum}")
	public String insertQue(QuestionVO questionvo) {
		examService.insertQues(questionvo);
		System.out.println(questionvo);
		return "redirect:/admin/adminExamView/{teNum}";
	}
	
	
	
}
