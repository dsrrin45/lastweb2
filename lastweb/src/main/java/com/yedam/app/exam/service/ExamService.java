package com.yedam.app.exam.service;

import java.util.List;

import com.yedam.app.config.Criteria;
import com.yedam.app.study.service.BigCateVO;
import com.yedam.app.study.service.CateVO;

public interface ExamService {
	public List<QuestionVO> questionList(int teNum);
	
	public List<TestVO> selectTest(Criteria criteria);
	
	public int getTestTotal();
	
	public TestVO getTest(int teNum);
	
	public int insertTest(TestVO testvo);
	
	public int updateQues(QuestionVO questionvo);
	
	public int daleteTest(int teNum);
	
	public int insertQues(QuestionVO questionvo);
	
	public List<QuestionVO> getSub(int teNum);
	
	public int selCateTotal(String bigkeyword, String keyword);
	
	public List<TestVO> testList(String bigkeyword, String keyword, Criteria criteria);
	
	public List<TestVO> examTest(String bigkeyword, String keyword);
	
	public List<TestVO> examList();
	
	public List<TestVO> examRound(int teYear);
	
	public List<QuestionVO> quizList(int teNum);
	
	//카테고리 대분류
	public List<BigCateVO> bigCateList();

	//카테고리 소분류
	public List<CateVO> cateList();
	
}
