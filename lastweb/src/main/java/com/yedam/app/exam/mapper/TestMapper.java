package com.yedam.app.exam.mapper;

import java.util.List;

import com.yedam.app.config.Criteria;
import com.yedam.app.exam.service.TestVO;
import com.yedam.app.study.service.BigCateVO;
import com.yedam.app.study.service.CateVO;

public interface TestMapper {
	public List<TestVO> selectTest(Criteria criteria);
	
	public int getTestTotal();
	
	public TestVO testInfo(int teNum);
	
	public int insertTest(TestVO testVO);
	
	public int deleteTest(int teNum);
	
	public int selCateTotal(String bigkeyword, String keyword);
	
	public List<TestVO> testList(String bigkeyword, String keyword, Criteria criteria);
	
	public List<TestVO> examTest(String bigkeyword, String keyword);
	
	public List<TestVO> examList();
	
	public List<TestVO> examRound(int teYear);
	
	//카테고리 대분류
	public List<BigCateVO> bigCateList();

	//카테고리 소분류
	public List<CateVO> cateList();
	
}
