package com.yedam.app.exam.service;

import java.util.Date;

import lombok.Data;

@Data
public class WrongVO {
	int noteNum;
	int myAnswer;
	String myMemo;
	Date myDate;
	int myscoreNum;
	int questionNum;
	String question;
	String questionSub;
	String vi1;
	String vi2;
	String vi3;
	String vi4;
	int answer;
	String commetary;
	int quesNum;
}
