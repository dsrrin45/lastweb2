package com.yedam.app.exam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.config.Criteria;
import com.yedam.app.exam.mapper.ExamMapper;
import com.yedam.app.exam.mapper.ScoreMapper;
import com.yedam.app.exam.mapper.WrongMapper;
import com.yedam.app.exam.service.QuestionVO;
import com.yedam.app.exam.service.ScoreService;
import com.yedam.app.exam.service.ScoreVO;
import com.yedam.app.exam.service.WrongVO;

@Service
public class ScoreServiceImpl implements ScoreService{
	@Autowired
	ScoreMapper scoreMapper;

	@Autowired
	WrongMapper wrongMapper;
	
	@Autowired
	ExamMapper examMapper;

	@Override
	public int insertScore(ScoreVO scoreVO) {
		List<QuestionVO> queList= examMapper.questionAllList(scoreVO.getTeNum());
		int scoreNum = scoreMapper.getScoreNum();
		scoreVO.setMyscoreNum(scoreNum);
		
		System.out.println(queList);
		int total = 0;
		//정답조회 
		if(scoreVO.getWrongList() != null) {
			for(int i=0; i<scoreVO.getWrongList().size(); i++ ) {
				//if != 틀리면
				WrongVO wrong = scoreVO.getWrongList().get(i);
				wrong.setMyscoreNum(scoreNum);
				wrongMapper.insertWrong(wrong);
				if(queList.get(i).getAnswer() == wrong.getMyAnswer()) {
					//맞으면 합계
					total += 10;
				}
			}
			
		}
		scoreVO.setMyscore(total/5);
		return scoreMapper.insertScore(scoreVO);
	}

	@Override
	public ScoreVO selectScore(int myscoreNum) {
		return scoreMapper.selectScore(myscoreNum);
	}

	@Override
	public List<ScoreVO> selectNote(String mbId,int pageNum, int amount) {
		List<ScoreVO> NoteList = scoreMapper.selectNote(mbId, pageNum, amount);
		return NoteList;
	}

	@Override
	public List<WrongVO> selectWrongList(int myscoreNum) {
		return wrongMapper.selectWrongList(myscoreNum);
	}

	@Override
	public int updateWrong(List<WrongVO> list) {
		int num = 0;
		for(int i=0; i<list.size(); i++) {
			wrongMapper.updateWrong(list.get(i));
			num++;
		}
		return num;
	}

	@Override
	public List<ScoreVO> getScore(ScoreVO scoreVO) {
		return scoreMapper.getScore(scoreVO);
	}


	@Override
	public int getNoteListTotal(String mbId) {
		int getNoteListTotal = scoreMapper.getNoteListTotal(mbId);
		return getNoteListTotal;
	}

	@Override
	public int selNoteCate(String bigkeyword, String keyword, String mbId) {
		return scoreMapper.selNoteCate(bigkeyword, keyword, mbId);
	}

	@Override
	public List<ScoreVO> noteCateList(String bigkeyword, String keyword, Criteria criteria,String mbId) {
		return scoreMapper.noteCateList(bigkeyword, keyword, criteria, mbId);
	}

	@Override
	public List<WrongVO> selectScoring(int myscoreNum) {
		return wrongMapper.selectScoring(myscoreNum);
	}








	



	


}
