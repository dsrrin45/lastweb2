package com.yedam.app.exam.mapper;

import java.util.List;

import com.yedam.app.config.Criteria;
import com.yedam.app.exam.service.ScoreVO;

public interface ScoreMapper {
	public int insertScore(ScoreVO scoreVO);
	
	public ScoreVO selectScore(int myscoreNum);
	
	public List<ScoreVO> selectNote(String mbId, int pageNum, int amount);
	
	public int getNoteListTotal(String mbId);
	
	public int getScoreNum();
	
	public List<ScoreVO> getScore(ScoreVO scoreVO);
	
	public int selNoteCate(String bigkeyword, String keyword, String mbId);
	
	public List<ScoreVO> noteCateList(String bigkeyword, String keyword, Criteria criteria,String mbId);
}
