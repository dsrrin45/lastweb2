package com.yedam.app.exam.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.yedam.app.exam.service.ExamService;
import com.yedam.app.exam.service.ScoreService;
import com.yedam.app.exam.service.TestVO;
import com.yedam.app.member.service.MbVO;

@Controller
public class QuizController {
	@Autowired
	ExamService examService;
	
	@Autowired
	ScoreService scoreService;
	
	@GetMapping("/quizList")
	public String quizList(Model model) {
		List<TestVO> test = examService.examList();
		model.addAttribute("exam", test);
		return "quiz/quizList";
	}
	
	@GetMapping("/quizYear")
	@ResponseBody
	public List<TestVO> quizRound(@RequestParam("teYear") int teYear) {
		List<TestVO> round = examService.examRound(teYear);
		return round;
	}
	
	@GetMapping("/quizCate")
	public String examList(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Model model) {
		model.addAttribute("pageMaker", examService.selCateTotal(bigkeyword, keyword));
		List<TestVO> testVO = examService.examTest(bigkeyword, keyword);
		model.addAttribute("exam", testVO);
		return "quiz/quizList";
	}
	
	@GetMapping("/quizView/{teNum}")
	public String QuestionList(@PathVariable int teNum, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if(mvo != null) {
			model.addAttribute("member", mvo);
			model.addAttribute("quizView", examService.quizList(teNum));
			model.addAttribute("test", examService.getTest(teNum));
			return "quiz/quizView";
		}else {
			return "redirect:/login";
		}
	}
	
	@GetMapping("/quizResult")
	public String getScore(@RequestParam("score") int score) {
		return "quiz/quizResult";
	}
	
	@PostMapping("/quizResult")
	@ResponseBody
	public int quizResult(@RequestParam("score") int score) {
		System.out.println(score);
	    return score;
	}
	
}
