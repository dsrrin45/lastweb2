package com.yedam.app.exam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.config.Criteria;
import com.yedam.app.exam.mapper.ExamMapper;
import com.yedam.app.exam.mapper.TestMapper;
import com.yedam.app.exam.service.ExamService;
import com.yedam.app.exam.service.QuestionVO;
import com.yedam.app.exam.service.TestVO;
import com.yedam.app.study.mapper.StudyMapper;
import com.yedam.app.study.service.BigCateVO;
import com.yedam.app.study.service.CateVO;

@Service
public class ExamServiceImpl implements ExamService {
	
	@Autowired
	ExamMapper examMapper;
	
	@Autowired
	TestMapper testMapper;
	
	@Autowired
	StudyMapper studyMapper;
	
	@Override
	public List<QuestionVO> questionList(int teNum) {
		return examMapper.questionAllList(teNum);
	}

	@Override
	public List<TestVO> selectTest(Criteria cri) {
		return testMapper.selectTest(cri);
	}

	@Override
	public TestVO getTest(int teNum) {
		return testMapper.testInfo(teNum);
	}

	@Override
	public int insertTest(TestVO testvo) {
		return testMapper.insertTest(testvo);
	}

	@Override
	public int updateQues(QuestionVO questionvo) {
		return examMapper.updateQues(questionvo);
	}


	@Override
	public int insertQues(QuestionVO questionvo) {
		return examMapper.insertQues(questionvo);
	}

	@Override
	public int daleteTest(int teNum) {
		return testMapper.deleteTest(teNum);
	}

	@Override
	public List<QuestionVO> getSub(int teNum) {
		return examMapper.getSub(teNum);
	}

	@Override
	public int getTestTotal() {
		int getTsetTotal = testMapper.getTestTotal();
		return getTsetTotal;
	}

	@Override
	public int selCateTotal(String bigkeyword, String keyword) {
		return testMapper.selCateTotal(bigkeyword, keyword);
	}

	@Override
	public List<TestVO> testList(String bigkeyword, String keyword, Criteria criteria) {
		return testMapper.testList(bigkeyword, keyword, criteria);
	}

	@Override
	public List<TestVO> examTest(String bigkeyword, String keyword) {
		return testMapper.examTest(bigkeyword, keyword);
	}

	@Override
	public List<TestVO> examList() {
		return testMapper.examList();
	}

	@Override
	public List<TestVO> examRound(int teYear) {
		return testMapper.examRound(teYear);
	}

	@Override
	public List<QuestionVO> quizList(int teNum) {
		return examMapper.quizList(teNum);
	}

	//카테고리 대분류
	@Override
	public List<BigCateVO> bigCateList() {
		return studyMapper.bigCateList();
	}

	//카테고리 소분류
	@Override
	public List<CateVO> cateList() {
		return studyMapper.cateList();
	}
	
	
}
