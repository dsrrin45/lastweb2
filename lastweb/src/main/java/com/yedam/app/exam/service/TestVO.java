package com.yedam.app.exam.service;


import lombok.Data;

@Data
public class TestVO {

	int teNum;
	int teYear;
	int teRound;
	int teTime;
	String testCheck;
	String bcaNum;
	String scaNum;
	int questionNum;
	String question;
	String questionSub;
	String vi1;
	String vi2;
	String vi3;
	String vi4;
	String answer;
	String commetary;
}
