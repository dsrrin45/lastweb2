package com.yedam.app.feed.service;

import java.util.List;

import com.yedam.app.study.service.StudyVO;


public interface FeedService {
	
//	// 페이징
//	public int getFeedListTotal();
	
	// 해당 스터디 피드(소모임) 참여자 조회
	public List<StudyVO> selectChatter(String studyNum);
	
	//피드 리스트조회
	public List<FeedVO> selectFeed(String studyNum);

	// 피드 등록
	public int insertFeed(FeedVO FeedVO);
	
	// 피드 댓글 등록
	public int insertFeedReply(FeedVO FeedVO);
	
	// 해당 유저
	public StudyVO selectUser(String studyNum);
	
	// 피드 삭제 
	public int delFeed(FeedVO FeedVO);
	
	// 피드 삭제 
	public int delChatterOut(StudyVO studyVO);
	
	// 해당 피드조회
	public FeedVO getFeed(FeedVO FeedVO);
	

//	// 페이지 수
//	public int getfeedListTotal();
//	
//	// 커뮤 상세조회
//	public CmVO selectCmInfo(String comNum);
//	public List<CmVO> selectCmReply(String comNum);
//	//삭제(수정)
//	public boolean deleteCmInfo(String comNum); 
//	public boolean modifyCm(CmVO cmVO);
//	
//	//댓글 리스트
//	public List<CmVO> getCmReplyList(String comNum);
//	// 댓글 등록
//	public int addCmReply(CmVO cmVO);
//	
	
}
