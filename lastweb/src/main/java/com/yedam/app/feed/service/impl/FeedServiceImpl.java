package com.yedam.app.feed.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.feed.mapper.FeedMapper;
import com.yedam.app.feed.service.FeedService;
import com.yedam.app.feed.service.FeedVO;
import com.yedam.app.study.service.StudyVO;

@Service
public class FeedServiceImpl implements FeedService {

	@Autowired
	FeedMapper feedMapper;
	
	
	@Override
	public List<StudyVO> selectChatter(String studyNum) {
		List<StudyVO> feedList = feedMapper.selectChatter(studyNum);
		
		
		return feedList;
	}


	@Override
	public List<FeedVO> selectFeed(String studyNum) {

		List<FeedVO> feedList = feedMapper.selectFeed(studyNum);
		
		//목록 가져올시 댓글 수 FeedVO 객체에 저장
		for (FeedVO feed : feedList) {
	        int replyCount = feedMapper.replyCount(feed.getFeedNum());
	        feed.setReplyCount(replyCount); // 피드 댓글 수 설정
	    }
		
		return feedList;
	}


	@Override
	public int insertFeed(FeedVO FeedVO) {
		
		int result = feedMapper.insertFeed(FeedVO);
		return result;
	}


	@Override
	public StudyVO selectUser(String studyNum) {
	
		return feedMapper.selectUser(studyNum);
	}


	@Override
	public int delFeed(FeedVO FeedVO) {
		
		int result = feedMapper.delFeed(FeedVO);
		return result;
	}


	@Override
	public int insertFeedReply(FeedVO FeedVO) {
		int result = feedMapper.insertFeedReply(FeedVO);
		return result;
	}


//	@Override
//	public int getFeedListTotal() {
//		int getFeedListTotal = feedMapper.getFeedListTotal();
//		return getFeedListTotal;
//	}


	@Override
	public int delChatterOut(StudyVO studyVO) {
		int result = feedMapper.delChatterOut(studyVO);
		if(result == 0)
			return result;
		
		// study_board 테이블에 참석자 수를 변경
		feedMapper.updateChatters(studyVO); 
		
		return result;
	}


	@Override
	public FeedVO getFeed(FeedVO FeedVO) {
		return feedMapper.getFeed(FeedVO);
	}





//	@Override
//	public int getfeedListTotal() {
//		int getfeedListTotal = feedMapper.getfeedListTotal();
//		return getfeedListTotal;
//	}





//
//	@Override
//	public List<CmVO> selectAllCmList(Criteria cri) {
//		List<CmVO> cmList = cmMapper.selectAllCmList(cri);
//		return cmList;
//				
//	}
//	
//	
//	@Override
//	public int getCmListTotal() {
//		int getCmListTotal = cmMapper.getCmListTotal();
//		return getCmListTotal;
//	}
//
//	//상세
//	@Override
//	public CmVO selectCmInfo(String comNum) {
//		return cmMapper.selectCmInfo(comNum);
//	}
//
//	@Override
//	public int insertCmInfo(CmVO cmVO) {
//		int result = cmMapper.insertCmInfo(cmVO);
//		
//		return result;
//	}
//
//	@Override
//	public boolean deleteCmInfo(String comNum) {
//		
//		boolean result = cmMapper.deleteCmInfo(comNum) == 1 ? true : false;
//		
//		return result;
//	}
//
//	@Override
//	public boolean modifyCm(CmVO cmVO) {
//		
//		return cmMapper.modifyCm(cmVO) == 1 ? true : false;
//	}
//
//	@Override
//	public List<CmVO> selectCmReply(String comNum) {
//
//		return cmMapper.selectCmReply(comNum);
//	}
//
//
//	@Override
//	public int addCmReply(CmVO cmVO) {
//		int result = cmMapper.addCmReply(cmVO);
//		return result;
//	}
//
//
//	@Override
//	public List<CmVO> getCmReplyList(String comNum) {
//		return cmMapper.getCmReplyList(comNum);
//	}
//

	
}
