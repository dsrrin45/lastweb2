package com.yedam.app.feed.mapper;

import java.util.List;

import com.yedam.app.feed.service.FeedVO;
import com.yedam.app.study.service.StudyVO;



public interface FeedMapper {
	
//	// 페이징
//	public int getFeedListTotal();
	
	// 해당 스터디 피드(소모임) 참여자 조회
	public List<StudyVO> selectChatter(String studyNum);
	
	//피드 리스트조회
	public List<FeedVO> selectFeed(String studyNum);
	
	// 해당 피드조회
	public FeedVO getFeed(FeedVO FeedVO);
	
	// 피드 등록
	public int insertFeed(FeedVO feedVO);
	
	// 피드 댓글 등록
	public int insertFeedReply(FeedVO FeedVO);
	
	// 해당 유저
	public StudyVO selectUser(String studyNum);
	
	// 피드 삭제 
	public int delFeed(FeedVO FeedVO);
	
	// 피드 댓글 수 
	public int replyCount(String feedNum);
	
	// 피드 탈퇴 
	public int delChatterOut(StudyVO studyVO);
	
	//스터디 참석자 수 변경
	public int updateChatters(StudyVO studyVO);
	
//	// 페이지 수
//	public int getfeedListTotal();
	
//	public List<CmVO> selectAllCmList(Criteria criteria);
//	//페이징
//	public int getCmListTotal();
//	
//	//상세
//	public CmVO selectCmInfo(String comNum);
//	public List<CmVO> selectCmReply(String comNum);
//	public int insertCmInfo(CmVO cmVO);
//	public int addCmReply(CmVO cmVO);
//	public int deleteCmInfo(String comNum);
//	public int modifyCm(CmVO cmVO);
//	//댓글리스트
//	public List<CmVO> getCmReplyList(String comNum);
	
	
}
