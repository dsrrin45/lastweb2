package com.yedam.app.feed.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class FeedVO {
	String feedNum;
	String bcaNum;
	String scaNum;
	String likeCount;
	String feedContent;
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
	Date feedWriteDate;
	String orgNum;
	String mbId;
	String studyNum;
	String mbNick;
	String deleted;
	String mbProfile;
	String level; // 댓글 들여쓰기
	int replyCount; // 피드 댓글 수
	String feedWriteDateChar; //날짜 형식변환한 데이터
	
}
