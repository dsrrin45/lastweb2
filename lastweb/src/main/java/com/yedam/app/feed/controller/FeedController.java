package com.yedam.app.feed.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.feed.service.FeedService;
import com.yedam.app.feed.service.FeedVO;
import com.yedam.app.study.service.StudyVO;

@Controller
public class FeedController {

	@Autowired
	FeedService feedService;

	//피드 페이지 호출
	@GetMapping("profile/myChatView/{studyNum}")
	public String myChatView(@PathVariable("studyNum") String studyNum, Model model) {	
		//참여자 리스트 조회
		model.addAttribute("chatterList", feedService.selectChatter(studyNum));
		model.addAttribute("user", feedService.selectUser(studyNum));
//		model.addAttribute("pageMaker", new PageDTO(feedService.getFeedListTotal(), 10, criteria));
		
		return "profile/myChatView";
	}
	
	//피드 페이지 데이터호출
	@GetMapping("/myFeed/{studyNum}")
	@ResponseBody
	public List<FeedVO> myFeed(@PathVariable("studyNum") String studyNum, Model model) {	
		
		return feedService.selectFeed(studyNum);
	}
	
	// 피드 게시글 등록
	@ResponseBody
	@RequestMapping(value = "/myFeed/insertFeed", method = RequestMethod.POST)
	public FeedVO insertFeed(FeedVO feedVO) {
		int result= feedService.insertFeed(feedVO);
		System.out.println(feedVO.getFeedNum());
		
		if(result>0) {
		
			return feedVO;
//			return feedService.getFeed(feedVO);
		} else {
			return null;
		}
	}
	
	// 피드 댓글 등록
	@ResponseBody
	@RequestMapping(value = "/myFeed/insertFeedReply", method = RequestMethod.POST)
	public FeedVO insertFeedReply(FeedVO feedVO) {
		int result= feedService.insertFeedReply(feedVO);
		if(result>0) {
			
			return feedVO;
		} else {
			return null;
		}
	}
	
	
//	// 피드 삭제 
	@ResponseBody
	@GetMapping("/delFeed/{feedNum}")
	public int feedDel(@PathVariable("feedNum") String feedNum, FeedVO feedVO) {
		int result = feedService.delFeed(feedVO);
		return result;
	}

	
	// 피드 탈퇴
	@ResponseBody
	@GetMapping("/delChatterOut/{studyNum}/{mbId}")
	public int delChatterOut(@PathVariable("studyNum") String studyNum, @PathVariable("mbId") String mbId, StudyVO studyVO) {
		int result = feedService.delChatterOut(studyVO);
		return result;
	}
	
	
	
//	
//	//커뮤니티 상세페이지
//	@GetMapping("/communityView/{comNum}")
//	public String comInfo(@PathVariable("comNum") String comNum, Model model, Criteria criteria){
//		model.addAttribute("cm", cmService.selectCmInfo(comNum));
//		model.addAttribute("reply", cmService.selectCmReply(comNum));
//		model.addAttribute("pageMaker", new PageDTO(cmService.getCmListTotal(), 10, criteria));
//		return "community/communityView";
//	}
//	
//	// 커뮤니티 게시글 삭제처리(update deleted)
//	@GetMapping("/communityDelete/{comNum}")
//	public String comInfoDelete(@PathVariable("comNum") String comNum) {
//		//model.addAttribute("cm", cmService.selectCmInfo(comNum));
//		cmService.deleteCmInfo(comNum);
//		return "redirect:/community";
//	}
//	
//	// 커뮤니티 수정페이지 이동
//		@GetMapping("/communityModify/{comNum}")
//		public String modifyCmPage(@PathVariable("comNum") String comNum, Model model) {
//			model.addAttribute("cm", cmService.selectCmInfo(comNum));
//			return "community/communityMod";
//		}
//	
//	// 커뮤니티 수정처리
//		@PostMapping("/communityModify")
//		public String modifyCm(CmVO cmVO) {
//			//model.addAttribute("cm", cmService.selectCmInfo(comNum));
//			System.out.println(cmVO);
//			cmService.modifyCm(cmVO);
//			return "redirect:communityView/" + cmVO.getComNum();
//		} 
//	
//
//	// 등록화면 페이지 호출
//	@GetMapping("/communityWrite")
//	public String inputCmForm() {	
//		return "community/communityWrite";
//	}
//	
//	//등록 처리
//	@PostMapping("/communityWrite")
//	public String inputCmProcess(@ModelAttribute CmVO cmVO) {	//처리화면(내부필드값으로 클래스를 받아옴)
//		cmService.insertCmInfo(cmVO);
//		System.out.println(cmVO);
//		return "redirect:community";			// 페이지가 아니라 경로에 대한 정보
//	}
//	
//	
//	//댓글 리스트 
//	@ResponseBody
//	@RequestMapping(value = "/cmReplyList/{comNum}")
//	public List<CmVO> qnaAnswerList(@PathVariable String comNum) {
//		return cmService.getCmReplyList(comNum);
//	}
//	
//	
//	
//	
//	//커뮤니티 댓글 등록
//	@ResponseBody
//	@RequestMapping(value = "/addCmReply", method = RequestMethod.POST)
//	//@PostMapping("/addCmReply")
//	public CmVO cmReply(CmVO cmVO) {
//		int result = cmService.addCmReply(cmVO);
//		if(result > 0) {
//			return cmVO;
//		}else {
//			return null;
//		}
//	}
//	
//	// 커뮤니티 댓글 삭제처리(update deleted)
//		@GetMapping("/delCmReply/{comNum}")
//		public String delCmReply(@PathVariable("comNum") String comNum) {
//			//model.addAttribute("cm", cmService.selectCmInfo(comNum));
////			cmService.delCmReply(comNum);
//			return "redirect:/communityView";
//		}
	

}
