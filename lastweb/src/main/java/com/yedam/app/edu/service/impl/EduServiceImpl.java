package com.yedam.app.edu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.edu.mapper.EduMapper;
import com.yedam.app.edu.service.EduBookmarkVO;
import com.yedam.app.edu.service.EduCommuVO;
import com.yedam.app.edu.service.EduInqVO;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.edu.service.EduPlanVO;
import com.yedam.app.edu.service.EduReviewVO;
import com.yedam.app.edu.service.EduService;
import com.yedam.app.edu.service.EduVO;
import com.yedam.app.edu.service.EduVideoVO;

@Service
public class EduServiceImpl implements EduService {
	@Autowired
	EduMapper eduMapper;

	@Override
	public List<EduVO> getAllList(Criteria criteria) {
		return eduMapper.selectAllList(criteria);
	}

	@Override
	public EduVO getClassInfo(String clNum) {
		return eduMapper.getClassInfo(clNum);
	}
	
	@Override
	public BookVO getClass(String clNum) {
		return eduMapper.getClass(clNum);
	}

	@Override
	public List<EduVO> searchList(String keyword, Criteria criteria) {
		return eduMapper.searchList(keyword, criteria);
	}

	@Override
	public List<EduVO> cateList(String keyword, String bigkeyword, Criteria criteria) {
		return eduMapper.cateList(bigkeyword, keyword, criteria);
	}

	@Override
	public List<EduVO> getInq(String clNum, Criteria criteria) {
		return eduMapper.getInq(clNum, criteria);
	}

	@Override
	public List<EduVO> getReview(String clNum, Criteria criteria) {
		return eduMapper.getReview(clNum, criteria);
	}

	@Override
	public List<EduVO> getcommu(String clNum, Criteria criteria) {
		return eduMapper.getcommu(clNum, criteria);
	}

	@Override
	public EduCommuVO commView(String clNum, String boNum) {
		return eduMapper.commView(clNum, boNum);
	}

	@Override
	public int addInq(EduInqVO eduInq) {
		return eduMapper.addInq(eduInq);
	}

	@Override
	public List<EduCommuVO> replyView(String clNum, String boNum) {
		return eduMapper.replyView(clNum, boNum);
	}

	@Override
	public int addRev(EduReviewVO eduRev) {
		return eduMapper.addRev(eduRev);
	}

	@Override
	public int inqDel(String inqNum) {
		return eduMapper.inqDel(inqNum);
	}

	@Override
	public int revDel(String reviewNum) {
		return eduMapper.revDel(reviewNum);
	}

	@Override
	public List<EduPlanVO> getClassBigPlan(String clNum) {
		List<EduPlanVO> list = eduMapper.getClassBigPlan(clNum);
		for(EduPlanVO vo : list) {
			vo.setCpList(eduMapper.getClassPlan(vo.getClpNum()));
		}
		 return list;
	}

	@Override
	public int addComm(EduCommuVO eduComm) {
		return eduMapper.addComm(eduComm);
	}

	@Override
	public int commDel(String boNum) {
		return eduMapper.commDel(boNum);
	}

	@Override
	public int CommModifyView(EduCommuVO eduCommuVO) {
		return eduMapper.CommModifyView(eduCommuVO);
	}
	
	@Override
	public int eduInqModify(EduInqVO eduInqVO) {
		return eduMapper.eduInqModify(eduInqVO);
	}

	@Override
	public int eduRevModify(EduReviewVO eduRiviewVO) {
		return eduMapper.eduRevModify(eduRiviewVO);
	}

	@Override
	public EduVideoVO eduVideoView(String clNum, String clpNum) {
		return eduMapper.eduVideoView(clNum, clpNum);
	}

	@Override
	public List<EduBookmarkVO> eduBookmark(String clNum, String clpNum) {
		return eduMapper.eduBookmark(clNum, clpNum);
	}

	@Override
	public int bookmarkDel(int bmNum) {
		return eduMapper.bookmarkDel(bmNum);
	}

	@Override
	public int addBookmark(EduBookmarkVO eduBookmark) {
		return eduMapper.addBookmark(eduBookmark);
	}

	@Override
	public int getTotal() {
		return eduMapper.getTotal();
	}

	@Override
	public int getInqTotal(String clNum) {
		return eduMapper.getInqTotal(clNum);
	}

	@Override
	public int getRevTotal(String clNum) {
		return eduMapper.getRevTotal(clNum);
	}

	@Override
	public int getCommuTotal(String clNum) {
		return eduMapper.getCommuTotal(clNum);
	}

	@Override
	public int saveClass(EduPersonalClassVO eduPersonalClassVO) {
		return eduMapper.saveClass(eduPersonalClassVO);
	}

	@Override
	public EduPersonalClassVO memberVideo(String clNum, String clpNum, String mbId) {
		return eduMapper.memberVideo(clNum, clpNum, mbId);
	}

	@Override
	public int getSeachTotal(String keyword) {
		return eduMapper.getSeachTotal(keyword);
	}

	@Override
	public int getCateTotal(String keyword, String bigkeyword) {
		return eduMapper.getCateTotal(keyword, bigkeyword);
	}

	@Override
	public int addCommReply(EduCommuVO eduComm) {
		return eduMapper.addCommReply(eduComm);
	}





}

