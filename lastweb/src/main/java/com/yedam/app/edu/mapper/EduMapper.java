package com.yedam.app.edu.mapper;

import java.util.List;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.edu.service.EduBookmarkVO;
import com.yedam.app.edu.service.EduCommuVO;
import com.yedam.app.edu.service.EduInqVO;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.edu.service.EduPlanVO;
import com.yedam.app.edu.service.EduReviewVO;
import com.yedam.app.edu.service.EduVO;
import com.yedam.app.edu.service.EduVideoVO;


public interface EduMapper {
	// 조회
	public List<EduVO> selectAllList(Criteria criteria);
	public EduVO getClassInfo(String clNum);
	public BookVO getClass(String clNum);
	public List<EduVO> searchList(String keyword, Criteria criteria);
	public List<EduVO> cateList(String keyword, String bigkeyword, Criteria criteria);
	public List<EduVO> getInq(String clNum, Criteria criteria);
	public List<EduVO> getReview(String clNum, Criteria criteria);
	public List<EduVO> getcommu(String clNum, Criteria criteria);
	public List<EduPlanVO> getClassPlan(String clNum);
	public EduCommuVO commView(String clNum, String boNum);
	public List<EduCommuVO> replyView(String clNum, String boNum);
	public List<EduPlanVO> getClassBigPlan(String clNum);
	public EduVideoVO eduVideoView(String clNum, String clpNum);
	public List<EduBookmarkVO> eduBookmark(String clNum, String clpNum);
	public EduPersonalClassVO memberVideo(String clNum, String clpNum, String mbId);
	
	// 등록
	public int addInq(EduInqVO eduInq);
	public int addRev(EduReviewVO eduRev);
	public int addComm(EduCommuVO eduComm);
	public int addBookmark(EduBookmarkVO eduBookmark);
	public int addCommReply(EduCommuVO eduComm);
	
	// 삭제
	public int inqDel(String inqNum);
	public int revDel(String reviewNum);
	public int commDel(String boNum);
	public int bookmarkDel(int bmNum);
	
	// 수정
	public int CommModifyView(EduCommuVO eduCommuVO);
	public int eduRevModify(EduReviewVO eduReviewVO);
	public int eduInqModify(EduInqVO eduInqVO);
	
	// 페이징
	public int getTotal();
	public int getInqTotal(String clNum);
	public int getRevTotal(String clNum);
	public int getCommuTotal(String clNum);
	public int getSeachTotal(String keyword);
	public int getCateTotal(String keyword, String bigkeyword);
	
	// 저장
	public int saveClass(EduPersonalClassVO eduPersonalClassVO);
}
