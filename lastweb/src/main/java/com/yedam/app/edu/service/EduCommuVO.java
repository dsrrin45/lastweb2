package com.yedam.app.edu.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EduCommuVO {
//	BO_NUM      NOT NULL VARCHAR2(20)  
//	BO_TITLE    NOT NULL VARCHAR2(50)  
//	BO_CONTENT  NOT NULL VARCHAR2(500) 
//	WRITE_DATE  NOT NULL DATE          
//	BO_STATE    NOT NULL CHAR(1)       
//	ORG_NUM              VARCHAR2(20)  
//	BO_CATEGORY NOT NULL VARCHAR2(20)  
//	MB_ID                VARCHAR2(20)  

	String boNum;
	String boTitle;
	String boContent;
	@DateTimeFormat(pattern="yyyy-MM-dd") // yyyy/MM/dd
	Date writeDate;
	String boState;
	String orgNum;
	String boCategory;
	String mbId;
	String clNum;
	String mbProfile;
}
