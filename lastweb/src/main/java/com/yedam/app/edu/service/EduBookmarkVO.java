package com.yedam.app.edu.service;

import lombok.Data;

@Data
public class EduBookmarkVO {
//	BM_NUM  NOT NULL NUMBER(3)     
//	BM_TIME          VARCHAR2(20)  
//	BM_MEMO          VARCHAR2(500) 
//	CLP_NUM          VARCHAR2(20)  
//	MB_ID            VARCHAR2(20)  
	int bmNum;
	String bmTime;
	String bmMemo;
	String clpNum;
	String mbId;
	String clNum;
}
