package com.yedam.app.edu.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EduVO {
//	CL_NUM       NOT NULL VARCHAR2(20)  
//	CL_THUMBNAIL NOT NULL VARCHAR2(50)  
//	CL_NAME      NOT NULL VARCHAR2(50)  
//	CL_GRADE     NOT NULL NUMBER(1)     
//	CL_AMOUNT    NOT NULL NUMBER(10)    
//	CL_INTRO              VARCHAR2(100) 
//	CL_START     NOT NULL DATE          
//	CL_REPORT             VARCHAR2(20)  
//	MB_ID        NOT NULL VARCHAR2(20)  
//	BCA_NUM               VARCHAR2(20)  
//	SCA_NUM               VARCHAR2(20)  

	String clNum;
	String clThumbnail;
	String clName;
	int clGrade;
	int clAmount;
	String clIntro;
	@DateTimeFormat(pattern="yyyy-MM-dd") // yyyy/MM/dd
	Date clStart;
	String clReport;
	String mbId;
	String mbNick;
	String bcaNum;
	String scaNum;
	
}
