package com.yedam.app.edu.service;

import lombok.Data;

@Data
public class EduVideoVO {
//	CLP_NUM     NOT NULL VARCHAR2(20)  
//	CLP_COUNT            NUMBER(3)     
//	CLP_VIDEO            VARCHAR2(100) 
//	CL_NUM               VARCHAR2(20)  
//	SMALL_TITLE          VARCHAR2(50)  
//	CLP_BIG_NUM          NUMBER(3)    
	
	String clpNum;
	int clpCount;
	String clpVideo;
	String clNum;
	String smallTitle;
	int clpBigNum;
	
	
}
