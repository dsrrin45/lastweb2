package com.yedam.app.edu.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.board.service.BoardVO;
import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.edu.service.EduBookmarkVO;
import com.yedam.app.edu.service.EduCommuVO;
import com.yedam.app.edu.service.EduInqVO;
import com.yedam.app.edu.service.EduPersonalClassVO;
import com.yedam.app.edu.service.EduReviewVO;
import com.yedam.app.edu.service.EduService;
import com.yedam.app.edu.service.EduVO;
import com.yedam.app.emp.service.EmpVO;
import com.yedam.app.member.service.MbVO;

@Controller
public class EduController {

	@Autowired
	EduService eduService;
	
	// 강의리스트
	@GetMapping("/edu")
	public String allList(Criteria criteria, Model model){
		model.addAttribute("eduList", eduService.getAllList(criteria));
		model.addAttribute("pageMaker", new PageDTO(eduService.getTotal(), 10, criteria));
		return "edu/edu";
	}
	
	// 강의리스트검색
	@GetMapping("/search")
	 public String search(@RequestParam("keyword") String keyword, Criteria criteria, Model model) {
	    model.addAttribute("eduList", eduService.searchList(keyword, criteria));
	    model.addAttribute("pageMaker", new PageDTO(eduService.getSeachTotal(keyword), 10, criteria));
	    return "edu/edu";
	 }
	
	// 강의리스트카테고리
	@GetMapping("/cateSearch")
	public String catesearch(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Criteria criteria, Model model) {
	    model.addAttribute("eduList", eduService.cateList(keyword, bigkeyword, criteria));
	    model.addAttribute("pageMaker", new PageDTO(eduService.getCateTotal(keyword, bigkeyword), 10, criteria));
	    return "edu/edu";
	} // http://localhost:8080/boot/cateSearch?keyword=기능사&bigkeyword=조리
	
	
	// 강의상세페이지, 문의
	@GetMapping("/eduInfo/{clNum}")
	public String eduInfo(@PathVariable("clNum") String clNum, Model model, Criteria criteria){
	    model.addAttribute("eduInfo", eduService.getClassInfo(clNum));
	    model.addAttribute("eduInq", eduService.getInq(clNum, criteria));
	    model.addAttribute("pageMaker", new PageDTO(eduService.getInqTotal(clNum), 10, criteria));
	    return "edu/eduInfo";
	}
	
	// 강의문의등록
	@PostMapping("/eduInqAdd")
	public String addInq(EduInqVO eduInqVO, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			System.out.println(eduInqVO);
			eduService.addInq(eduInqVO);
			return "redirect:eduInfo/" + eduInqVO.getClNum(); // 해당 경로로 호출
		} else {
			return "redirect:/login";
		}
	}
	
	// 강의문의수정
	@ResponseBody
	@PostMapping("/eduInqModify")
	public String eduInqModify(EduInqVO eduInqVO) {
		eduService.eduInqModify(eduInqVO);
		System.out.println(eduInqVO);
		return "redirect:eduInfo/" + eduInqVO.getClNum();
	}
	
	// 강의문의삭제
	@ResponseBody
	@GetMapping("/eduInqDel/{inqNum}")
	public String eduDel(@PathVariable("inqNum") String inqNum) {
		eduService.inqDel(inqNum);
		return "edu/edu";
	}
	
	// 강의계획페이지
	@GetMapping("/eduPlan/{clNum}")
	public String eduPlan(@PathVariable("clNum") String clNum, Model model, Criteria criteria){
	    model.addAttribute("eduInfo", eduService.getClassInfo(clNum));
	    model.addAttribute("eduClassPlan", eduService.getClassBigPlan(clNum));
	    return "edu/eduPlan";
	}
	
	// 강의후기페이지
	@GetMapping("/eduReview/{clNum}")
	public String eduReview(@PathVariable("clNum") String clNum, Model model, Criteria criteria){
	    model.addAttribute("eduInfo", eduService.getClassInfo(clNum));
	    model.addAttribute("edureview", eduService.getReview(clNum, criteria));
	    model.addAttribute("pageMaker", new PageDTO(eduService.getRevTotal(clNum), 10, criteria));
	    return "edu/eduReview";
	}

	// 강의후기등록
	@PostMapping("/eduRevAdd")
	public String addRev(EduReviewVO eduRevVO, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			System.out.println(eduRevVO);
			eduService.addRev(eduRevVO);
			return "redirect:eduReview/" + eduRevVO.getClNum();
		} else {
			return "redirect:/login";
		}
	}
	
	// 강의후기수정
	@ResponseBody
	@PostMapping("/eduRevModify")
	public String eduRevModify(EduReviewVO eduReviewVO) {
		eduService.eduRevModify(eduReviewVO);
		System.out.println(eduReviewVO);
		return "redirect:eduReview/" + eduReviewVO.getClNum();
	}
	
	// 강의후기삭제
	@ResponseBody
	@GetMapping("/eduRevDel/{reviewNum}")
	public void RevDel(@PathVariable("reviewNum") String reviewNum) {
		eduService.revDel(reviewNum);
	}
	
	// 강의커뮤니티페이지
	@GetMapping("/eduCommu/{clNum}")
	public String eduCommu(@PathVariable("clNum") String clNum, Model model, Criteria criteria){
		model.addAttribute("eduInfo", eduService.getClassInfo(clNum));
		model.addAttribute("educommu", eduService.getcommu(clNum, criteria));
	    model.addAttribute("pageMaker", new PageDTO(eduService.getCommuTotal(clNum), 10, criteria));
	    return "edu/eduCommunity";
	}

	// 강의커뮤상세조회
	@GetMapping("/eduCommuView/{clNum}/{boNum}") // 나중에 /eduCommu/{}형태로 변경
	public String eduCommuView(@PathVariable("clNum") String clNum, @PathVariable("boNum") String boNum, Model model) {
		model.addAttribute("eduView", eduService.commView(clNum, boNum));
		model.addAttribute("replyView", eduService.replyView(clNum, boNum));
		return "edu/eduCommView";
	}
	
	// 강의커뮤등록페이지
	@GetMapping("/eduCommunityWrite")
	public String eduCommunityWrite(@RequestParam("clNum") String clNum, Model model) {
		model.addAttribute("clNum", clNum);
		return "edu/eduCommunityWrite";
	}

	// 강의커뮤등록
	@PostMapping("/addComm")
	public String addComm(EduCommuVO eduCommuVO, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			System.out.println(eduCommuVO);
			eduService.addComm(eduCommuVO);
			return "redirect:eduCommu/" + eduCommuVO.getClNum();
		} else {
			return "redirect:/login";
		}
	}
	
	// 강의커뮤수정으로 이동
	@GetMapping("/eduCommModify/{clNum}/{boNum}")
	public String eduCommModify(@PathVariable("clNum") String clNum, @PathVariable("boNum") String boNum, Model model) {
		model.addAttribute("eduView", eduService.commView(clNum, boNum));
		return "edu/eduCommModify";
	}

	// 강의커뮤삭제
	@GetMapping("/eduCommDel/{orgNum}/{boNum}")
	public String CommDel(@PathVariable("orgNum") String orgNum, @PathVariable("boNum") String boNum) {
	    eduService.commDel(boNum);
	    return "redirect:eduCommu/" + orgNum;
	}
	
	// 강의커뮤수정
	@PostMapping("/eduCommModify")
	public String eduCommModify(EduCommuVO eduCommuVO) {
		eduService.CommModifyView(eduCommuVO);
		System.out.println(eduCommuVO);
		return "redirect:eduCommu/" + eduCommuVO.getOrgNum();
	}
	
	// 강의커뮤리플등록
	@PostMapping("/addCommReply")
	public String addCommReply(EduCommuVO eduCommuVO) {
		eduService.addCommReply(eduCommuVO);
		System.out.println(eduCommuVO);
		return "redirect:eduCommu/" + eduCommuVO.getOrgNum();
	}
	
	// 강의문의삭제
	@ResponseBody
	@GetMapping("/commReplyDel/{boNum}")
	public String commReplyDel(@PathVariable("boNum") String boNum) {
		eduService.commDel(boNum);
		return "edu/edu";
	}
	
	// 강의비디오페이지
	@GetMapping("/eduVideoView/{clNum}/{clpNum}/{mbId}") 
	public String eduVideoView(@PathVariable("clNum") String clNum, @PathVariable("clpNum") String clpNum, @PathVariable("mbId") String mbId, Model model) {
		model.addAttribute("clNum", clNum);
		model.addAttribute("clpNum", clpNum);
	    model.addAttribute("eduClassPlan", eduService.getClassBigPlan(clNum));
	    model.addAttribute("eduVideoView", eduService.eduVideoView(clNum, clpNum));
	    model.addAttribute("eduBookmark", eduService.eduBookmark(clNum, clpNum));
	    model.addAttribute("memberVideo", eduService.memberVideo(clNum, clpNum, mbId));
	    return "edu/eduVideoView";
	}

	// 강의북마크등록
	@PostMapping("/addBookmark")
	public String addBookmark(EduBookmarkVO eduBookmarkVO, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mvo = (MbVO) session.getAttribute("login");
		if (mvo != null) {
			System.out.println(eduBookmarkVO);
			eduService.addBookmark(eduBookmarkVO);
			return "redirect:eduVideoView/" + eduBookmarkVO.getClNum() + '/' + eduBookmarkVO.getClpNum(); // 해당 경로로 호출
		} else {
			return "redirect:/login";
		}
	}

	// 강의북마크삭제
	@PostMapping("/bookmarkDel")
	@ResponseBody
	public String bookmarkDel(@RequestParam("bmNum") int bmNum, @RequestParam("clpNum") int clpNum,  @RequestParam("clNum") int clNum) {
	    eduService.bookmarkDel(bmNum);
	    return "success";
	}
	
	// 강의자동저장
	@PostMapping("/saveClass")
	@ResponseBody
	public int saveClass(EduPersonalClassVO eduPersonalClassVO) {
		int result = eduService.saveClass(eduPersonalClassVO);
		return result;
	}
	
	// 마이비디오리스트(승인)
	@GetMapping("/MyVideosList")
	public String MyVideosList() {
		return "teacher/MyVideosList";
	}
	
	// 마이비디오등록
	@GetMapping("/MyVideosForm")
	public String MyVideosForm() {
		return "teacher/MyVideosForm";
	}
	
	// 마이비디오상세
	@GetMapping("/MyVideoInfo")
	public String MyVideoInfo() {
		return "teacher/MyVideoInfo";
	}
	
	// 더미
	@GetMapping("/teacherMyVideoList")
	public String teacherMyVideoList() {
		return "teacher/teacherMyVideoList";
	}
	
	// 업로드테스트(미작동)
	@GetMapping("/upload")
	public String upload() {
		return "edu/upload";
	}  
	

}
