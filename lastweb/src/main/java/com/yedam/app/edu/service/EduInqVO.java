package com.yedam.app.edu.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EduInqVO {
//	INQ_NUM     NOT NULL VARCHAR2(20)  
//	INQ_CONTENT          VARCHAR2(500) 
//	INQ_TIME             DATE          
//	INQ_ANS              VARCHAR2(500) 
//	MB_ID                VARCHAR2(20)  
//	CL_NUM               VARCHAR2(20)  
//	CLP_NUM              VARCHAR2(20)  

	String inqNum;
	String inqContent;
	@DateTimeFormat(pattern="yyyy-MM-dd") // yyyy/MM/dd
	Date inqTime;
	String inqAns;
	String mbId;
	String clNum;
	String clpNum;
	int secret;
	String mbProfile;
}
