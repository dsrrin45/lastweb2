package com.yedam.app.edu.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EduReviewVO {
//	GOODS_NUM               VARCHAR2(20)  
//	REVIEW_GRADE            NUMBER(1)     
//	REVIEW_CONTENT          VARCHAR2(500) 
//	REVIEW_NUM     NOT NULL VARCHAR2(20)  
//	MB_ID                   VARCHAR2(20)  
//	REPORT_NUM              VARCHAR2(20)  
//	DEF                     CHAR(1)     

	String goodsNum;
	String clNum;
	int reviewGrade;
	String reviewContent;
	String reviewNum;
	String mbId;
	String reportNum;
	String def;
	@DateTimeFormat(pattern="yyyy-MM-dd") // yyyy/MM/dd
	Date reviewTime;
	String mbProfile;
}
