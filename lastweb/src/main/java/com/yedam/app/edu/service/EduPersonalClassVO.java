package com.yedam.app.edu.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
public class EduPersonalClassVO {
//	PC_NUM  NOT NULL NUMBER(3)    
//	PC_PROG          NUMBER(5,2)  
//	MB_ID            VARCHAR2(20) 
//	CL_NUM           VARCHAR2(20) 
//	CLPNUM           VARCHAR2(20) 
//	PC_TIME          NUMBER(5,2)  
	int pcNum;
	float pcProg;
	String mbId;
	String clNum;
	String clpNum;
	float pcTime;
	
	String clThumbnail;
	String clName;
	int clGrade;
	int clAmount;
	String clIntro;
	@DateTimeFormat(pattern="yyyy-MM-dd") // yyyy/MM/dd
	Date clStart;
	String clReport;
	String mbNick;
	String bcaNum;
	String scaNum;
}
