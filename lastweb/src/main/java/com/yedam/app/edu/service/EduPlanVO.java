package com.yedam.app.edu.service;

import java.util.List;

import lombok.Data;

@Data
public class EduPlanVO {
	// CLP_NUM     NOT NULL VARCHAR2(20)  
	// CLP_COUNT            NUMBER(3)     
	// CLP_VIDEO            VARCHAR2(100) 
	// CL_NUM               VARCHAR2(20)  
	// BIG_TITLE            VARCHAR2(50)  
	// SMALL_TITLE          VARCHAR2(50)  
	
	String clpNum;
	int clpCount;
	String clpVideo;
	String clNum;
	String bigTitle;
	String smallTitle;
	List<EduPlanVO> cpList; 
}
