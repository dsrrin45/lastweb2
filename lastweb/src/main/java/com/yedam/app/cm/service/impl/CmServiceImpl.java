package com.yedam.app.cm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.cm.mapper.CmMapper;
import com.yedam.app.cm.service.CmService;
import com.yedam.app.cm.service.CmVO;
import com.yedam.app.config.Criteria;

@Service
public class CmServiceImpl implements CmService {

	@Autowired
	CmMapper cmMapper;

	@Override
	public List<CmVO> selectAllCmList(Criteria cri) {
		List<CmVO> cmList = cmMapper.selectAllCmList(cri);
		return cmList;

	}

	@Override
	public int getCmListTotal() {
		int getCmListTotal = cmMapper.getCmListTotal();
		return getCmListTotal;
	}

	// 상세
	@Override
	public CmVO selectCmInfo(String comNum) {
		return cmMapper.selectCmInfo(comNum);
	}

	@Override
	public int insertCmInfo(CmVO cmVO) {
		int result = cmMapper.insertCmInfo(cmVO);

		return result;
	}

	@Override
	public boolean deleteCmInfo(String comNum) {

		boolean result = cmMapper.deleteCmInfo(comNum) == 1 ? true : false;

		return result;
	}

	@Override
	public boolean modifyCm(CmVO cmVO) {

		return cmMapper.modifyCm(cmVO) == 1 ? true : false;
	}

	@Override
	public List<CmVO> selectCmReply(String comNum) {

		return cmMapper.selectCmReply(comNum);
	}

	@Override
	public int addCmReply(CmVO cmVO) {
		int result = cmMapper.addCmReply(cmVO);
		return result;
	}

	@Override
	public List<CmVO> getCmReplyList(String comNum) {
		return cmMapper.getCmReplyList(comNum);
	}

	@Override
	public int delCmReply(CmVO cmVO) {
		int result = cmMapper.delCmReply(cmVO);
		return result;
	}

	@Override
	public int modifyCmReply(CmVO cmVO) {
		int result = cmMapper.modifyCmReply(cmVO);
		return result;
	}

	@Override
	public List<CmVO> searchCate(String bigkeyword, String keyword, Criteria criteria) {
		return cmMapper.searchCate(bigkeyword, keyword, criteria);
	}

	@Override
	public int getCateTotal(String keyword, String bigkeyword) {
		return cmMapper.getCateTotal(keyword, bigkeyword);
	}

}
