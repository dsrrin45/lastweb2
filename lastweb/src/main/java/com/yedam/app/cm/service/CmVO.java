package com.yedam.app.cm.service;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class CmVO {
	String comNum;
	String bcaNum;
	String scaNum;
	String comTitle;
	String comContent;
//	@DateTimeFormat(pattern="yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
	Date comWriteDate;
	String NoticeState;
	String orgNum;
	String mbId;
	String reportNum;
	String mbNick;
	String deleted;
	String reply;
	String mbProfile;
	
}
