package com.yedam.app.cm.mapper;

import java.util.List;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.cm.service.CmVO;
import com.yedam.app.config.Criteria;

public interface CmMapper {
	public List<CmVO> selectAllCmList(Criteria criteria);

	// 페이징
	public int getCmListTotal();
	
	// 커뮤니티 목록 카테고리 검색
	public List<CmVO> searchCate(String bigkeyword, String keyword, Criteria criteria);
	public int getCateTotal(String keyword, String bigkeyword);

	// 상세
	public CmVO selectCmInfo(String comNum);

	public List<CmVO> selectCmReply(String comNum);

	public int insertCmInfo(CmVO cmVO);

	public int addCmReply(CmVO cmVO);

	public int deleteCmInfo(String comNum);

	public int modifyCm(CmVO cmVO);

	// 댓글 리스트
	public List<CmVO> getCmReplyList(String comNum);

	// 댓글 삭제
	public int delCmReply(CmVO cmVO);

	// 댓글 수정
	public int modifyCmReply(CmVO cmVO);
}
