package com.yedam.app.cm.service;

import java.util.List;

import com.yedam.app.book.service.BookVO;
import com.yedam.app.config.Criteria;

public interface CmService {
	public List<CmVO> selectAllCmList(Criteria criteria);

	// 페이지 수
	public int getCmListTotal();
	
	// 목록 카테고리 검색
	public List<CmVO> searchCate(String bigkeyword, String keyword, Criteria criteria);
	public int getCateTotal(String keyword, String bigkeyword);

	// 커뮤 상세조회
	public CmVO selectCmInfo(String comNum);

	public List<CmVO> selectCmReply(String comNum);

	// 게시글 등록
	public int insertCmInfo(CmVO cmVO);

	// 삭제(수정)
	public boolean deleteCmInfo(String comNum);

	public boolean modifyCm(CmVO cmVO);

	// 댓글 리스트
	public List<CmVO> getCmReplyList(String comNum);

	// 댓글 등록
	public int addCmReply(CmVO cmVO);

	// 댓글 삭제
	public int delCmReply(CmVO cmVO);

	// 댓글 수정
	public int modifyCmReply(CmVO cmVO);
}