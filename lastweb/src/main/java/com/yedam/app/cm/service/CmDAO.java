package com.yedam.app.cm.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.yedam.app.cm.mapper.CmMapper;
import com.yedam.app.config.Criteria;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class CmDAO {

private final CmMapper cmMapper;
	
	public List<CmVO> selectAllCmList(Criteria cri) {
		return cmMapper.selectAllCmList(cri);
	}
	

}
