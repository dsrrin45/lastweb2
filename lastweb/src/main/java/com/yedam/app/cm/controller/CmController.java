package com.yedam.app.cm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yedam.app.cm.service.CmService;
import com.yedam.app.cm.service.CmVO;

import com.yedam.app.config.Criteria;
import com.yedam.app.config.PageDTO;
import com.yedam.app.study.service.StudyService;

@Controller
//@RestController
public class CmController {

	@Autowired
	CmService cmService;
	
	@Autowired
	StudyService studyService;

	// 커뮤니티목록
	@GetMapping("/community")
	// @ResponseBody
	public String selectAllComuList(Criteria criteria, Model model) {
		model.addAttribute("communityList", cmService.selectAllCmList(criteria));
		model.addAttribute("pageMaker", new PageDTO(cmService.getCmListTotal(), 10, criteria));
		return "community/communityList";
	}
	
	// 커뮤니티 목록 카테고리 검색
	@GetMapping("/communityCate")
	public String catesearch(@RequestParam("keyword") String keyword, @RequestParam("bigkeyword") String bigkeyword, Criteria criteria, Model model) {
	    model.addAttribute("communityList", cmService.searchCate(keyword, bigkeyword, criteria));
	    model.addAttribute("pageMaker", new PageDTO(cmService.getCateTotal(keyword, bigkeyword), 10, criteria)); 
	    return "community/communityList";
	} 
	
	
	

	// 커뮤니티 상세페이지
	@GetMapping("/communityView/{comNum}")
	public String comInfo(@PathVariable("comNum") String comNum, Model model, Criteria criteria) {
		model.addAttribute("cm", cmService.selectCmInfo(comNum));
		model.addAttribute("pageMaker", new PageDTO(cmService.getCmListTotal(), 10, criteria));
		return "community/communityView";
	}

	// 커뮤니티 게시글 삭제처리(update deleted)
	@GetMapping("/communityDelete/{comNum}")
	public String comInfoDelete(@PathVariable("comNum") String comNum) {
		// model.addAttribute("cm", cmService.selectCmInfo(comNum));
		cmService.deleteCmInfo(comNum);
		return "redirect:/community";
	}

	// 커뮤니티 수정페이지 이동
	@GetMapping("/communityModify/{comNum}")
	public String modifyCmPage(@PathVariable("comNum") String comNum, Model model) {
		model.addAttribute("cm", cmService.selectCmInfo(comNum));
		return "community/communityMod";
	}

	// 커뮤니티 수정처리
	@PostMapping("/communityModify")
	public String modifyCm(CmVO cmVO) {
		// model.addAttribute("cm", cmService.selectCmInfo(comNum));
		System.out.println(cmVO);
		cmService.modifyCm(cmVO);
		return "redirect:communityView/" + cmVO.getComNum();
	}

	// 등록화면 페이지 호출
	@GetMapping("/communityWrite")
	public String inputCmForm(Model model) {
		model.addAttribute("bigCateList", studyService.bigCateList());
		model.addAttribute("cateList", studyService.cateList());
		return "community/communityWrite";
	}

	// 등록 처리
	@PostMapping("/communityWrite")
	public String inputCmProcess(@ModelAttribute CmVO cmVO) { // 처리화면(내부필드값으로 클래스를 받아옴)
		cmService.insertCmInfo(cmVO);
		return "redirect:community"; // 페이지가 아니라 경로에 대한 정보
	}

	// 댓글 리스트
	@ResponseBody
	@RequestMapping(value = "/cmReplyList/{comNum}")
	public List<CmVO> cmReplyList(@PathVariable String comNum) {
		return cmService.getCmReplyList(comNum);
	}

	// 커뮤니티 댓글 등록
	@ResponseBody
	@RequestMapping(value = "/addCmReply", method = RequestMethod.POST)
	public CmVO cmReply(CmVO cmVO) {
		int result = cmService.addCmReply(cmVO);
		if (result > 0) {
			return cmVO;
		} else {
			return null;
		}
	}

	// 커뮤니티 댓글 삭제처리
	@ResponseBody
	@RequestMapping(value = "/delCmReply", method = RequestMethod.POST)
	public int delCmReply(CmVO cmVO) {
		int result = cmService.delCmReply(cmVO);
		return result;
	}
	
	// 커뮤니티 댓글 수정처리
	@ResponseBody
	@RequestMapping(value = "/modifyCmReply", method = RequestMethod.POST)
	public int modifyCmReply(CmVO cmVO) {
		int result = cmService.modifyCmReply(cmVO);
		return result;
	}
}
