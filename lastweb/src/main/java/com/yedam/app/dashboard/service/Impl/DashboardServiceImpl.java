package com.yedam.app.dashboard.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yedam.app.dashboard.mapper.DashboardMapper;
import com.yedam.app.dashboard.service.DashboardService;
import com.yedam.app.dashboard.service.DashboardVO;
import com.yedam.app.edu.service.EduVO;

@Service
public class DashboardServiceImpl implements DashboardService {
	@Autowired
	DashboardMapper dashboardMapper;
	
	@Override
	public EduVO myClass(String mbId) {
		EduVO eduVO = dashboardMapper.myClass(mbId);
		return eduVO;
	}

	@Override
	public List<DashboardVO> myClassCount(String mbId) {
		List<DashboardVO> dashboardVO = dashboardMapper.myClassCount(mbId);
		return dashboardVO;
	}

	@Override
	public List<DashboardVO> allClassCount() {
		List<DashboardVO> dashboardVO = dashboardMapper.allClassCount();
		return dashboardVO;
	}

}
