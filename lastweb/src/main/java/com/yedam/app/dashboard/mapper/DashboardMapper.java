package com.yedam.app.dashboard.mapper;

import java.util.List;

import com.yedam.app.dashboard.service.DashboardVO;
import com.yedam.app.edu.service.EduVO;

public interface DashboardMapper {
	public EduVO myClass(String mbId);

	public List<DashboardVO> myClassCount(String mbId);

	public List<DashboardVO> allClassCount();
}
