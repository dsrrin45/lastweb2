package com.yedam.app.dashboard.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.yedam.app.dashboard.service.DashboardService;
import com.yedam.app.member.service.MbVO;

@Controller
public class DashboardController {
	@Autowired
	DashboardService dashboardService;

	// 마이 페이지
	@GetMapping("/mypage/mypageDashboard")
	public String mypageDashboard(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("myClass", dashboardService.myClass(mbVO.getMbId()));
		return "mypage/mypageDashboard";
	}

	// 강사 페이지
	@GetMapping("/teacher/teacherDashboard")
	public String teacherDashboard(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("myClass", dashboardService.myClassCount(mbVO.getMbId()));
		return "teacher/teacherDashboard";
	}
	
	// 관리자 페이지
	@GetMapping("/admin/adminDashboard")
	public String adminDashboard(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("myClass", dashboardService.allClassCount());
		return "admin/adminDashboard";
	}
 
	@GetMapping("/accessDenied")
	public String accessDenied() {
		return "index";
	}
}
