package com.yedam.app.dashboard.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.yedam.app.edu.service.EduVO;

@Service
public interface DashboardService {

	public EduVO myClass(String mbId);

	public List<DashboardVO> myClassCount(String mbId);

	public List<DashboardVO> allClassCount();
}
