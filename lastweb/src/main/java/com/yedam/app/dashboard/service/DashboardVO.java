package com.yedam.app.dashboard.service;

import lombok.Data;

@Data
public class DashboardVO {
	String clName;
	int count;
	int clAmount;
}
