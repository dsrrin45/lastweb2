package com.yedam.app.member.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.yedam.app.member.service.MbService;
import com.yedam.app.member.service.MbVO;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
public class MbController {

	@Autowired
	MbService mbService;

	// 로그인 폼
	@GetMapping("/login")
	public String loginForm(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if (session.getAttribute("login") != null) {
			return "redirect:/";
		} else {
			return "login/login";
		}
	}

	// 회원가입 폼
	@GetMapping("/join")
	public String inputMemberForm() {
		return "login/join";
	}

	// 회원가입
	@PostMapping("/join")
	public String inputMember(MbVO mbVO) {
		mbService.insertMember(mbVO);
		return "redirect:login";
	}

	// 아이디 중복 체크
	@ResponseBody
	@RequestMapping(value = "/idChk", method = RequestMethod.POST)
	public int idChk(MbVO mbVO) {
		int result = mbService.idChk(mbVO);
		return result;
	}

	// 이메일 발송
	@ResponseBody
	@RequestMapping(value = "/emailChk", method = RequestMethod.POST)
	public void emailChk(String mbEmail, String mbId) {
		String title = "[ 자격있는 사람들 ] 임시 비밀번호 발송";
		String tempPassword = getTempPassword(6);
		String body = "<h1>임시 비밀번호 : " + tempPassword + "</h1>";
		mbService.sendMail(mbEmail, title, body);
		int result = mbService.changePw(tempPassword, mbId);
		System.out.println(result);
	}

	public static String getTempPassword(int length) {
		int index = 0;
		char[] charArr = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
				'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			index = (int) (charArr.length * Math.random());
			sb.append(charArr[index]);
		}
		return sb.toString();
	}

	// 아이디찾기
	@GetMapping("/findId")
	public String searchIdForm() {
		return "login/findId";
	}

	@ResponseBody
	@RequestMapping(value = "/searchId", method = RequestMethod.POST)
	public String searchId(MbVO mbVO) {
		String result = mbService.findId(mbVO);
		return result;
	}

	// 비번 찾기
	@GetMapping("/findPw")
	public String searchPwForm() {
		return "login/findPw";
	}

	@ResponseBody
	@RequestMapping(value = "/searchPw", method = RequestMethod.POST)
	public int searchPw(MbVO mbVO) {
		int result = mbService.findPw(mbVO);
		return result;
	}

	// 관리자 페이지 - 회원 정보 관리
	@GetMapping("/admin/adminStudentList")
	public String studentList(Model model) {
		model.addAttribute("studentList", mbService.getStudentList());
		return "admin/adminStudentList";
	}

	// 관리자 페이지 - 강사 정보 관리
	@GetMapping("/admin/adminTeacherList")
	public String teacherList(Model model) {
		model.addAttribute("teacherList", mbService.getTeacherList());
		return "admin/adminTeacherList";
	}

	// 관리자 페이지 - 강사 등록
	@GetMapping("/admin/adminTeacherForm")
	public String teacherForm(Model model) {
		return "admin/adminTeacherForm";
	}

	// 마이 페이지 - 내 정보 관리
	@GetMapping("mypage/mypageMyInfoView")
	public String mypageMyInfoView(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("myInfo", mbService.getMypageMyInfo(mbVO));
		return "mypage/mypageMyInfoView";
	}

	// 마이 페이지 - 내정보 관리 수정 폼
	@GetMapping("mypage/mypageMyInfoForm")
	public String mypageMyInfoForm(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("myInfo", mbService.getMypageMyInfo(mbVO));
		return "mypage/mypageMyInfoForm";
	}

	@PostMapping("mypage/mypageMyInfoModify")
	public String mypageMyInfoModify(MbVO mbVO) {
		mbService.modifyInfo(mbVO);
		return "redirect:mypageMyInfoView";
	}

	// 마이 페이지 - 프로필 이미지 업로드
	@ResponseBody
	@RequestMapping(value = "/uploadAjaxAction", method = RequestMethod.POST)
	public void uploadAjaxPost(MultipartFile[] uploadFile, HttpServletRequest request) {
		String uploadFolder = "D:\\imageSaveStorage";
		for (MultipartFile multipartFile : uploadFile) {
			if (multipartFile != null && multipartFile.getSize() > 0) {
				String uploadFileName = multipartFile.getOriginalFilename();
				uploadFileName = uploadFileName.substring(uploadFileName.lastIndexOf("\\") + 1);
				File saveFile = new File(uploadFolder, uploadFileName);
				try {
					multipartFile.transferTo(saveFile);
					HttpSession session = request.getSession();
					MbVO mbVO = (MbVO) session.getAttribute("login");

					String contextPath = request.getContextPath();
					mbService.changeProfile(contextPath + "/images/" + uploadFileName, mbVO.getMbId());
				} catch (Exception e) {
				}
			}
		}
	}

	// 마이 페이지 - 회원 탈퇴
	@ResponseBody
	@RequestMapping(value = "/resign", method = RequestMethod.POST)
	public int resign(HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		int result = mbService.resign(mbVO);
		if (result > 0) {
			SecurityContextHolder.clearContext();
		}
		return result;
	}

	// 강사 페이지 - 내 정보 관리
	@GetMapping("teacher/teacherMyInfoView")
	public String teacherMyInfoView(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("myInfo", mbService.getMypageMyInfo(mbVO));
		return "teacher/teacherMyInfoView";
	}

	// 강사 페이지 - 내정보 관리 수정 폼
	@GetMapping("teacher/teacherMyInfoForm")
	public String teacherMyInfoForm(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		MbVO mbVO = (MbVO) session.getAttribute("login");
		model.addAttribute("myInfo", mbService.getMypageMyInfo(mbVO));
		return "teacher/teacherMyInfoForm";
	}

	@PostMapping("teacher/teacherMyInfoModify")
	public String teacherMyInfoModify(MbVO mbVO) {
		mbService.modifyTeacherInfo(mbVO);
		return "redirect:teacherMyInfoView";
	}
}