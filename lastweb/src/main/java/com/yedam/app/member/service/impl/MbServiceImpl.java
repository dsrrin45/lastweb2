package com.yedam.app.member.service.impl;

import java.util.List;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.yedam.app.member.mapper.MbMapper;
import com.yedam.app.member.service.MbService;
import com.yedam.app.member.service.MbVO;

@Service
public class MbServiceImpl implements MbService, UserDetailsService {

	@Autowired
	MbMapper mbMapper;
    
	@Autowired
	public JavaMailSender javaMailSender;
	
	public void sendMail(String memEmail, String title, String body) {
		MimeMessage message = javaMailSender.createMimeMessage();
		
		try {
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
    
        // 1. 메일 수신자 설정
        messageHelper.setTo(memEmail);

        // 2. 메일 제목 설정
        messageHelper.setSubject(title);

        // 3. 메일 내용 설정
        // HTML 적용됨
        String content = body;
        messageHelper.setText(content,true);

        // 4. 메일 전송
        javaMailSender.send(message);
		}catch(Exception e) {
		}
	}

	@Override
	public MbVO login(MbVO mbVO) {
		MbVO memberVO = mbMapper.login(mbVO);
		return memberVO;
	}
	
	@Override
	public int insertMember (MbVO mbVO) {
		int result = mbMapper.insertMember(mbVO);
		return result;
	}

	@Override
	public int idChk(MbVO mbVO) {
		int result = mbMapper.idChk(mbVO);
		return result;
	}

	@Override
	public String findId(MbVO mbVO) {
		String result = mbMapper.findId(mbVO);
		return result;
	}
	
	@Override
	public int findPw(MbVO mbVO) {
		int result = mbMapper.findPw(mbVO);
		return result;
	}
	
	@Override
	public List<MbVO> getStudentList() {
		List<MbVO> memberList = mbMapper.getStudentList();
		return memberList;
	}

	@Override
	public List<MbVO> getTeacherList() {
		List<MbVO> memberList = mbMapper.getTeacherList();
		return memberList;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		MbVO mbVO = new MbVO();
		mbVO.setMbId(username);
		MbVO vo = mbMapper.login(mbVO);
		if(vo == null) {
			throw new UsernameNotFoundException("no user");
		}
		return vo;
	}

	@Override
	public int changePw(String tempPassword, String mbId) {
		int result = mbMapper.changePw(tempPassword, mbId);
		return result;
	}

	@Override
	public MbVO getMypageMyInfo(MbVO mbVO) {
		MbVO result = mbMapper.login(mbVO);
		return result;
	}

	@Override
	public int modifyInfo(MbVO mbVO) {
		int result = mbMapper.modifyInfo(mbVO);
		return result;
	}

	@Override
	public int changeProfile(String uploadFileName, String mbId) {
		int result = mbMapper.changeProfile(uploadFileName, mbId);
		return result;
	}

	@Override
	public int resign(MbVO mbVO) {
		int result = mbMapper.resign(mbVO);
		return result;
	}

	@Override
	public int modifyTeacherInfo(MbVO mbVO) {
		int result = mbMapper.modifyTeacherInfo(mbVO);
		return result;		
	}
}
