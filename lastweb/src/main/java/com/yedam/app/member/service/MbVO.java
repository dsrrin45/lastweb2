package com.yedam.app.member.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;

@Data
public class MbVO implements UserDetails {
	String mbName;
	String mbNick;
	String mbId;
	String mbPw;
	int mbPost;
	String mbAddress1;
	String mbAddress2;
	String mbTel;
	String mbEmail;
	int mbPower;
	String mbState;
	String mbProfile;
	int mbMileage;
	String mbIntd;
	int loginState;
	String mbBank;
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> auth = new ArrayList<>();
		String role = "";
		if(mbPower == 0) {
			role = "ROLE_MEMBER";
		} else if (mbPower == 1) {
			role = "ROLE_TEACHER";
		} else if (mbPower == 2) {
			role = "ROLE_ADMIN";
		}
		auth.add(new SimpleGrantedAuthority(role));
		return auth;
	}

	@Override
	public String getPassword() {
		return mbPw;
	}

	@Override
	public String getUsername() {
		return mbId;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
