package com.yedam.app.member.mapper;

import java.util.List;

import com.yedam.app.member.service.MbVO;

public interface MbMapper {
	public MbVO login(MbVO mbVO);

	public int insertMember(MbVO mbVO);

	public int idChk(MbVO mbVO);

	// 아이디 찾기
	public String findId(MbVO mbVO);
	
	// 비밀번호 찾기
	public int findPw(MbVO mbVO);

	// 관리자 페이지 - 회원 정보 관리
	public List<MbVO> getStudentList();

	// 관리자 페이지 - 강사 정보 관리
	public List<MbVO> getTeacherList();

	public int changePw(String tempPassword, String mbId);

	public int modifyInfo(MbVO mbVO);

	// 프로필 사진 변경
	public int changeProfile(String uploadFileName, String mbId);

	// 회원 탈퇴
	public int resign(MbVO mbVO);

	public int modifyTeacherInfo(MbVO mbVO);
}
