package com.yedam.app.member.service;

import java.util.List;

public interface MbService {
	public MbVO login(MbVO mbVO);

	public int insertMember(MbVO mbVO);

	public int idChk(MbVO mbVO);
	
	// 아이디 찾기
	public String findId(MbVO mbVO);

	public void sendMail(String memEmail, String title, String body);

	// 비밀번호 찾기
	public int findPw(MbVO mbVO);
	
	// 관리자 페이지 - 회원 정보 관리
	public List<MbVO> getStudentList();

	// 관리자 페이지 - 강사 정보 관리
	public List<MbVO> getTeacherList();

	// 비밀번호 변경
	public int changePw(String tempPassword, String mbId);

	// 내 정보 호출
	public MbVO getMypageMyInfo(MbVO mbVO);

	public int modifyInfo(MbVO mbVO);
	
	// 프로필 사진 변경
	public int changeProfile(String uploadFileName, String mbId);

	// 회원 탈퇴
	public int resign(MbVO mbVO);

	// 내 정보 수정
	public int modifyTeacherInfo(MbVO mbVO);
}